<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package stockathon-blog
 */

get_header();
?>
	<!-- <section class="inner-page-banner">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div>
					<h1>Search Result</h1>
          <?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}?>
				</div>
			</div>
		</div>
	</div> -->
</section>
	<main id="primary" class="site-main searchbar">
	<section class="blog-section ">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8 blog-heading p-0">
					<div class="row blog-links">
		<?php if ( have_posts() ) : ?>

		
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;
			wp_pagenavi(array());
			//the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	
</div>


</div>
<div class="col-xl-4 col-lg-4 col-md-6">
	<div class="search-box">
		<?php get_search_form(); ?>
		</div>
		<div class="blog-categories">
		<?php get_sidebar(); ?>
		</div>
</div>
</div>
</div>
</section>
</main><!-- #main -->

<?php
get_footer();