<?php
  /**
   * The main template file
   *
   * This is the most generic template file in a WordPress theme
   * and one of the two required files for a theme (the other being style.css).
   * It is used to display a page when nothing more specific matches a query.
   * E.g., it puts together the home page when no home.php file exists.
   *
   * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
   *
   * @package stockathon-blog
   */
  
  get_header();
  $catHindi = 126;
  ?>
<!-- Blog Banner -->
<!-- <section class="inner-page-banner">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div>
					<h1>Blog</h1>

					<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>
</section> -->
<section class="blog-section">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-6">
				<?php 
          // $category = get_category_by_slug( 'category-name' );
          $args = array(
          'type'                     => 'post',
          // 'child_of'                 => $category->term_id,
          'orderby'                  => 'name',
          'order'                    => 'DESC',
          'hide_empty'               => false,
          'hierarchical'             => 1,
          'taxonomy'                 => 'category',
          ); 
          $child_categories = get_categories($args );
          $category_list = array();
          $category_list[] = $category->term_id;
          
          if ( !empty ( $child_categories ) ){
           foreach ( $child_categories as $child_category ){
               $category_list[] = $child_category->term_id;
           }
          }
          $posts_args = array(
          'posts_per_page'   => 1,
          // 'cat'  =>  $catHindi,
          'post_type'        => 'post',
          'post_status'      => 'publish',
          'suppress_filters' => true 
          );
          
          $posts = new WP_Query ( $posts_args );
          if ( $posts->have_posts() ){
          
          while ( $posts->have_posts() ){
           $posts->the_post(); 
           $category_array = array();
           $post_categories = get_the_category ( get_the_ID() );
           if ( !empty ( $post_categories ) ){
              foreach ( $post_categories as $post_category ) {
                 $category_array[] = $post_category->term_id;
               }
           }
           $result = array_diff( $category_array,  $category_list   ); 
          $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "size" );
          
           if ( empty ( $result ) ) { ?>
				<div class="blog-heading">

          <?php if($thumbnail == ""){ ?>

        <?php  } else { ?>
          
          <a href="<?php the_permalink(); ?>" class="text-center">
						<img src="<?php echo $thumbnail[0]; ?>" alt="Image" class="img-fluid m-auto fullwidth" style="display:block" />
          </a>
          
          <?php } ?>
					<div class="blog-detail-box">
						<ul>
							<li><i class="fas fa-user"></i> <?php the_author_meta( 'user_nicename'); ?> </li>
							<li><i class="far fa-calendar-alt"></i> <?php  	 echo get_the_date('M d, Y'); ?> </li>
							<li class="cmtcount"><i class="fas fa-comment-dots"></i>
								<?php
											comments_popup_link( '0', '1 comment', '%', 'comments-link', '');
										?>
							</li>
							<!-- <li>
								<i class="fas fa-external-link-square-alt"></i> <?php 
									$categories1 = get_the_category();
										if ( !empty( $categories1 ) ) {
												foreach( $categories1 as $category1 ) { ?>
								<?php  echo $category1->name ?>,
								<?php   } } ?>
							</li> -->
						</ul>
						<h5 class="text-capitalize" > <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a></h5>
						<?php 
              $content = get_the_content();
              $content = strip_tags($content);
              ?>
						<p><?php  if(strlen($content) >25){ echo substr($content, 0, 650).'...'; } else { echo $content; }   ?> </p>
						<a href="<?php the_permalink(); ?>" class="readmore">Read more</a>
					</div>
				</div>
				<?php
          }
        }
      }
        wp_reset_postdata();
		  ?>
				<!-- <h2 class="featured-blog-h2">Must Read</h2> -->
				<div class="col-md-12 p-0">
					<div class="featured-blog">
						<?php 
          // $category = get_category_by_slug( 'category-name' );
          $args = array(
          'type'                     => 'post',
          'child_of'                 => $category->term_id,
          'order'                    => 'DESC',
          'hide_empty'               => false,
          'hierarchical'             => 1,
          'taxonomy'                 => 'category',
          ); 
          $child_categories = get_categories($args );
          $category_list = array();
          $category_list[] = $category->term_id;
          
          if ( !empty ( $child_categories ) ){
           foreach ( $child_categories as $child_category ){
               $category_list[] = $child_category->term_id;
           }
          }
          
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $posts_args = array(
			  'posts_per_page'   =>10,
					
          // 'cat'  =>  $catHindi,
          'post_type'        => 'post',
          'post_status'      => 'publish',
           'paged'             => $paged,
          );
          
          $posts = new WP_Query ( $posts_args );
          if ( $posts->have_posts() ){
          
          while ( $posts->have_posts() ){
           $posts->the_post(); 
           $category_array = array();
           $post_categories = get_the_category ( get_the_ID() );
           if ( !empty ( $post_categories ) ){
              foreach ( $post_categories as $post_category ) {
                 $category_array[] = $post_category->term_id;
               }
           }
           $result = array_diff( $category_array,  $category_list   ); 
          $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "thumbnail" );
          $thumbnail_mobile = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium" );
          
           if ( empty ( $result ) ) {
             
            if($thumbnail != ''){
              $class = 'col-lg-9';
              $hideClass = '';
            }else{
              $class = 'col-lg-12';
              $hideClass = 'd-none';
            }
            ?>

						<div class="blog-feature-box row ml-0 mr-0">
							<div class="col-lg-3 pr-lg-0 <?php echo $hideClass;?>">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<div class="img-center">
                    <img src="<?php echo $thumbnail[0]; ?>" alt="image" class="img-fluid desktop-thumbanail " />
                    <img src="<?php echo $thumbnail_mobile[0]; ?>" alt="image" class="img-fluid mobile-thumbanail" />
									</div>
								</a>
							</div>
							<div class="col-lg-9 p-0 <?php echo $class; ?>">
								<div class="feature-box-detail">
									<ul>
										<li class="text-capitalize"><i class="fas fa-user"></i> <?php the_author_meta( 'user_nicename'); ?>
										</li>
										<li>
											<i class="far fa-calendar-alt"></i> <?php echo get_the_date('M d, Y'); ?>
										</li>
										<li class="cmtcount"><i class="fas fa-comment-dots"></i>
											<?php
											comments_popup_link( '0', '1 comment', '%', 'comments-link', '');
										?>
										</li>
										<!-- <li>
											<i class="fas fa-external-link-square-alt"></i>
											<?php 
                        $categories3 = get_the_category();
                        if ( !empty( $categories3 ) ) {
                        foreach( $categories3 as $category3 ) { ?>
											<?php  echo $category3->name ?>,
											<?php   } } ?>
										</li> -->
									</ul>
									<a class="text-capitalize" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php echo mb_strimwidth(get_the_title(), 0, 80, '...'); ?> </a>
									<?php 
                    $content = get_the_content();
                    $content = strip_tags($content);
                  ?>
									<p>
										<?php  if(strlen($content) >25){ echo substr($content, 0, 250).'...'; } else { echo $content; }   ?>
									</p>
								</div>
							</div>
						</div>
						<?php
          }
        }
      }
          //  end pagination of news post
        wp_pagenavi( array( 'query' => $posts ) );
        wp_reset_postdata();
      ?>
    </div>
  </div>
</div>
			<div class="col-xl-4 col-lg-4 col-md-6">
				<div class="search-box">
          <?php get_search_form(); ?>
          </div>
          <div class="blog-categories">
          <?php get_sidebar(); ?>
          </div>
			</div>
		</div>
	</div>
</section>

<?php

get_footer();