<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stockathon-blog
 */

get_header();
?>

<main id="primary" class="site-main">

	<?php if ( have_posts() ) : ?>

	<!-- <section class="inner-page-banner">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div>
						<?php if(is_category( 'Featured' )) : ?>
						<h1>Featured:</h1>
						<?php  else: ?>
						<h1>Category : <?php single_cat_title(); ?> </h1>
						<?php endif; ?>

						<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<section class="blog-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-6 blog-heading">
					<div class="row blog-links">

						<?php
						
			while ( have_posts() ) :
				the_post();

			
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;
			wp_pagenavi(array());
			// the_posts_navigation();
			
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
					</div>


				</div>
				<!-- Side bar hindi -->
				<div class="col-xl-4 col-lg-4 col-md-6">
				<div class="search-box">
          <?php get_search_form(); ?>
          </div>
          <div class="blog-categories">
          <?php get_sidebar(); ?>
          </div>
			</div>
			</div>
		</div>
	</section>
</main><!-- #main -->

<?php
get_footer();