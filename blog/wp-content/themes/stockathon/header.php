<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head>

  * * @link
  * * @package stockathon-blog */ ?>
  <!DOCTYPE html>
  <html <?php language_attributes(); ?>
    >

    <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1 maximum-scale=1.0, user-scalable=no"
      />
      <link rel="icon" href="" type="image/png" />
      <link
        rel="stylesheet"
        href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css"
      />
      <link
        rel="stylesheet preload"
        as="style"
        href="https://pro.fontawesome.com/releases/v5.8.2/css/all.css"
      />
      <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap"
        rel="stylesheet"
      />
      <link
        href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&family=Open+Sans:wght@400;600;700;800&display=swap"
        rel="stylesheet"
      />
      <link
        rel="stylesheet"
        type="text/css"
        href="<?php echo get_template_directory_uri() ?>/css/blog-header.css"
      />
      <link
        rel="stylesheet"
        type="text/css"
        href="<?php echo get_template_directory_uri() ?>/css/blog-style.css"
      />
      <link
        rel="stylesheet"
        type="text/css"
        href="<?php echo get_template_directory_uri() ?>/css/blog-responsive.css"
      />
      <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>
      >
      <?php wp_body_open(); ?>
      <div id="page" class="site">
        
        <!-- Header Start -->
        <section class="header-section justify-content-center align-items-center">
          <nav class="navbar navbar-expand-lg ">
            <?php
							$header_image = get_header_image();
							if (!empty($header_image)) : ?>
            <a
              href="<?php echo site_url(); ?>"
              class="navbar-brand"
              title="<?php echo bloginfo('name') ?>"
            >
              <img
              style="width: 200px;"
                src="<?php echo esc_url($header_image); ?>"
                title="<?php echo bloginfo('name') ?>"
                alt="<?php echo bloginfo('name') ?>"
            /></a>
            <?php endif; ?>
            <!-- <ul class="navbar-nav ml-auto"  id="mobilemenu">
                <?php wp_nav_menu( array('theme_location' => 'header_menu',  'menu_class' => 'navbar-nav ml-auto') ); ?>
                <div class="form-inline my-2 my-lg-0">
                  <a href="#" class="btn btn-outline-success my-2 my-sm-0 " type="submit">Login / Register</a>
                </div>
              </ul> -->
                <ul class="navbar-nav ml-auto" id="mobilemenu">
              <li class="nav-item">
                <a href="#" class="nav-link" title="Features">Features</a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link" title="Screens">Screens</a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link" title="Premium">Premium</a>
              </li>
              <!-- <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Dropdown on Right</a
                >
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                </div>
              </li> -->
            </ul>
            <!-- Login / Register -->
            <div class="form-inline my-2 my-lg-0" id="user_btn">
              <a href="https://www.stockathon.com/" class="btn btn-outline-success my-2 my-sm-0">Login / Register</a>
            </div>
          </nav>
          <!-- Menu icon  -->
         <button type="button" id="menuClick">
          <i class="fal fa-bars"></i>
         </button>
        </section>
        <!-- Header End -->
      </div>
</div>
