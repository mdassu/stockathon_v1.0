<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stockathon-blog
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}

	if ( have_comments() ) :
		?>
<div class="comment-box">
	<h5 class="comments-title">
		<?php
			$stockathon_blog_comment_count = get_comments_number();
			if ( '1' === $stockathon_blog_comment_count ) {
				
			} else {
				printf( 
					esc_html( _nx( '%1$s Comments', '%1$s Comments', $stockathon_blog_comment_count, '', 'stockathon-blog' ) ),
					number_format_i18n( $stockathon_blog_comment_count 
					
					)
				
				);
			}
			?>
	</h5><!-- .comments-title -->

	<?php the_comments_navigation(); ?>

	<ol class="comment-list">
		<?php
			wp_list_comments(
				array(
					'style'      => 'ol',
					'short_ping' => true,
				)
			);
			?>
	</ol><!-- .comment-list -->
		</div>
	<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
	<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'stockathon-blog' ); ?></p>
	<?php
		endif;

	endif; // Check for have_comments().

	?>
	<?php comment_form();?>




<!-- #comments -->