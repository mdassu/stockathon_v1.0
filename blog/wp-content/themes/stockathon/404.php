<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package stockathon-blog
 */

get_header();
?>
<section class="page-not-found">
  <div class="notfound">
    <img src="<?php echo get_template_directory_uri() ?>/images/404.svg" alt="" />
  </div>
  <div class="not-found-contetnt">
    <h1>Error 404: Page Not Found</h1>
    <p>
    We couldn't find the page you are looking for.
		</p>
		<a href="<?php echo get_site_url() ?>">Back To Home</a>

  </div>
</section>

		

<?php
get_footer();
