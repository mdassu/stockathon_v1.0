<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stockathon-blog
 */

get_header();
?>
<!-- <section class="inner-page-banner">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div>

					<h1><?php echo the_title(); ?></h1>

					<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
?>

				</div>
			</div>
		</div>
	</div>
</section> -->
<!-- End Blog Banner -->
<main id="primary" class="site-main">

	<div class="blog-heading">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- <?php //stockathon_blog_post_thumbnail(); ?> -->
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="feature-box-detail">
						<div class="blog-detail-box detail-page-box program-detail">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main><!-- #main -->

<?php
get_footer();