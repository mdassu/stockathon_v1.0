<? php
  /**
   * The template for displaying the footer
   *
   * Contains the closing of the #content div and all content after.
   *
   * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
   *
   * @package stockathon-blog
   */

  ?>

  <section class="copyright-section">
    <div class="container">
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12">
          <div class="copyright-left text-center">
            <p> &copy; <?<span>Stockathon. </span> All Rights Reserved </p>
          </div>
        </div>
        <div class="col-xl-12 col-lg-12 col-md-12 mt-4">
          <div class="terms text-center">
            <ul><li><a title="Home" href="">Home</a></li>
              <li><a title="About Us" href="#">About Us</a></li>
              <li><a title="Support" href="#">Support</a></li>
              <li><a title="Premium" href="#">Premium</a></li>
              <li><a title="What's New" href="#">What's New</a></li>
              <li><a title="Privacy" href="#">Privacy</a></li>
              <li><a title="Terms" href="#">Terms</a></li></ul>
          </div>
        </div>
      </div>
    </div>
  </section>
</div >

<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri() ?>/js/jquery-3.5.1.slim.min.js"></script>
  <script src="<?php echo get_template_directory_uri() ?>/js/popper.min.js"></script>
  <script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri() ?>/js/typeahead.jquery.min.js"></script>
  
  <script>
    // Navbar toggle
$(".navbar-toggler").click(function() {
  $(this).toggleClass("change");
});



// Sidebar layout
$('#menuClick').click(function(e) {
    e.preventDefault(); 
    e.stopPropagation();
    $("#mobilemenu").toggleClass("activeMenu");
    $(".fa-bars").toggleClass("fa-times");
});
  $('#mobilemenu').click(function(e) {
    e.stopPropagation(); 
});

$('body').click(function() {
  $("#mobilemenu").removeClass("activeMenu");
  $(".fa-bars").removeClass("fa-times");

});


// Navbar Fixed
$(window).scroll(function() {
  if ($(this).scrollTop() > 120) {
    $(".header-section").addClass("header-fixed");
  } else {
    $(".header-section").removeClass("header-fixed");
  }
});
//
// window.onchnage = function(){
//         location.href=document.getElementById("selectbox").value;
//     }   

    $(".dropdown-menu li a").click(function(){
  var selText = $(this).text();
  $(this).parents('.languagechange').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
});


$("body").on("click", function(event) {
  event.stopPropagation();
  // $(".menuResponsive").toggleClass("show");
});

  </script>

</body >
</html >
