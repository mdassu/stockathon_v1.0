<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stockathon-blog
 */

?>



<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'stockathon-blog' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>
<div class="page-not-found border-0 temp-center w-100">
	<div class="not-found-contetnt">



	<section class="page-not-found">
  <div class="notfound">
    <img src="<?php echo get_template_directory_uri() ?>/images/404.svg" alt="" />
  </div>
  <div class="not-found-contetnt">
    <h1>Nothing to see here!</h1>
    <p>
			Sorry, but nothing matched your search terms. <br> Please try again with some different keywords.
		</p>
		<a href="<?php echo get_site_url() ?>">Back To Home</a>

  </div>
</section>
<!-- <div class="search-box w-50 mx-auto">
			<?php get_search_form(); ?>
		</div>

		<h1>Nothing to see here!</h1>
		<p>
			Sorry, but nothing matched your search terms. Please try again with some different keywords.
		</p>
		<div class="search-box w-50 mx-auto">
			<?php get_search_form(); ?>
		</div> -->

	</div>
</div>



<?php
		

		else :
			?>

<div class="page-not-found border-0  temp-center w-100">
	<div class="not-found-contetnt">
		<h1>Nothing to see here!</h1>
		<p>
			Sorry, but nothing matched your search terms. Please try again with some different keywords.
		</p>
		<div class="search-box w-50 mx-auto">
			<?php get_search_form(); ?>
		</div>

	</div>
</div>
<?php

		endif;
		?>