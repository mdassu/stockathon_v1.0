<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stockathon-blog
 */
$imageBlog = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
$thumbnail_mobile = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
if($imageBlog != ''){
	$class = 'col-lg-9';
	$hideClass = '';
}else{
	$class = 'col-lg-12 pl-2';
	$hideClass = 'd-none';
}

?>


<div class=" col-lg-12 p-0 mb-3">
	<div class="blog-detail-box detail-page-box">
		<div class="row">

			<div class="col-lg-3 pr-lg-0 <?php echo $hideClass;?>">
				<a href="<?php the_permalink(); ?>" title="<?php echo the_title(); ?>">
					<div class="img-center">
						<img class="img-fluid desktop-thumbanail" src="<?php echo $imageBlog[0] ?>" alt="">
						<img src="<?php echo $thumbnail_mobile[0]; ?>" alt="image" class="img-fluid mobile-thumbanail" />
					</div>
				</a>
			</div>
			<div class="col-lg-9 p-0 <?php echo $class; ?>">
				<div class="feature-box-detail">
					<ul>
						<li><i class="fas fa-user"></i> <?php the_author_meta( 'user_nicename'); ?> </li>
						<li>
							<i class="far fa-calendar-alt"></i> <?php echo get_the_date('M d, Y'); ?>
						</li>
						<li class="cmtcount"><i class="fas fa-comment-dots"></i>
							<?php
									comments_popup_link( '0', '1 comment', '%', 'comments-link', '');
								?>
						</li>
						<li>
							<i class="fas fa-external-link-square-alt"></i> <?php 
							
							$categories2 = get_the_category();
								if ( !empty( $categories2 ) ) {
										foreach( $categories2 as $category2 ) { ?>
							<?php  echo $category2->name ?>,
							<?php   } } ?>
						</li>
					</ul>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php echo mb_strimwidth(get_the_title(), 0, 80, '...'); ?> </a>
					<?php
							$content = get_the_content();
							$content = strip_tags($content);
							?>
					<p>
						<?php  if(strlen($content) >5){ echo substr($content, 0, 250).'...'; } else { echo $content; }   ?>
					</p>
				</div>
			</div>
		</div>
	</div>

</div>