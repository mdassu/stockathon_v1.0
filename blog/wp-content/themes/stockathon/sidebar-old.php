<?php
  /**
   * The sidebar containing the main widget area
   *
   * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
   *
   * @package stockathon-blog
   */
  
  
  
  $catHindi =  126;
  
  
  ?>

  <!-- Default  hindi blog -->
  
<div class="search-box">
 
<?php get_search_form(); ?>
  <!-- <input type="text" placeholder="Search" />
  <i class="far fa-search search-icon"></i> -->
</div>
<div class="blog-categories">
  <?php echo $ID ?>
  <h5>
    <img src="<?php echo get_template_directory_uri() ?>/images/notice-before-img.png" alt="icon" /> Categories
  </h5>
  <!-- Hidin cate -->
  <ul>
    <?php

    
      $this_category = get_category($cat);
      // echo $this_category->cat_ID;
      $parent_term_id =$this_category->cat_ID; // term id of parent term
      
      
      //$termchildren = get_terms('category',array('child_of' => $parent_id));
      $taxonomies = array( 
      'taxonomy' => 'category'
      );
      $args = array(
      // 'parent'         => $parent_term_id,
       'child_of'      => $catHindi
      ); 
      $terms = get_terms($taxonomies, $args);
      if (sizeof($terms)>0)
      {
      foreach ( $terms as $term ) {
      $term_link = sprintf( '<li><a href="%1$s" alt="%2$s">%3$s</a></li>', esc_url( get_category_link( $term->term_id ) ),
      		esc_attr( $term->name ),
      		esc_html( $term->name ));
          echo sprintf( $term_link );
      }
      }
      ?>
  </ul>
</div>
<div class="blog-categories">
  <h5>
    <img src="<?php echo get_template_directory_uri() ?>/images/notice-before-img.png" alt="icon" /> Recent Posts
  </h5>
  <div class="recent-posts">
    <ul>
      <?php
        $args = array(
        'type'                     => 'post',
        'child_of'                 => $category->term_id,
        'orderby'                  => 'name',
        'order'                    => 'DESC',
        'hide_empty'               => false,
        'hierarchical'             => 1,
        'taxonomy'                 => 'category',
        ); 
        $child_categories = get_categories($args );
        $category_list = array();
        $category_list[] = $category->term_id;
        
        if ( !empty ( $child_categories ) ){
        foreach ( $child_categories as $child_category ){
            $category_list[] = $child_category->term_id;
        }
        }
        
        $posts_args = array(
        'posts_per_page'   => 5,
        'cat'  => $catHindi,
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => true ,
        'offset' => 1
        );
        
        $posts = new WP_Query ( $posts_args );
        if ( $posts->have_posts() ){
        while ( $posts->have_posts() ){
        $posts->the_post(); 
        $category_array = array();
        $post_categories = get_the_category ( get_the_ID() );
        if ( !empty ( $post_categories ) ){
           foreach ( $post_categories as $post_category ) {
              $category_array[] = $post_category->term_id;
            }
        }
        $result = array_diff( $category_array,  $category_list   ); 
        $image_arr = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'thumbnail');
        $image_url = $image_arr[0]; // $image_url is your URL.
        
        if ( empty ( $result ) ) { ?>
      <li>
        <div class="row">
          <div class="col-lg-4 col-4">
            <div class="img-center">
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <img src="<?php echo  $image_url; ?>" alt="img" />
              </a>
            </div>
          </div>
          <div class="col-lg-8 col-8 p-0">
            <div class="recent-blog-title">
              <span> <?php  	 echo get_the_date('M d, Y'); ?></span><br />
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
            </div>
          </div>
        </div>
      </li>
      <?php
        }
        }
        }
        wp_reset_postdata();
        ?>
    </ul>
  </div>
</div>
<div class="blog-categories">
  <h5>
    <img src="<?php echo get_template_directory_uri() ?>/images/notice-before-img.png" alt="icon" /> Popular Posts
  </h5>
  <div class="recent-posts">
    <ul>
      <?php
        $args = array(
        'type'                     => 'post',
        'child_of'                 => $category->term_id,
        'orderby'                  => 'name',
        'order'                    => 'DESC',
        'hide_empty'               => false,
        'hierarchical'             => 1,
        'taxonomy'                 => 'category',
        ); 
        $child_categories = get_categories($args );
        $category_list = array();
        $category_list[] = $category->term_id;
        
        if ( !empty ( $child_categories ) ){
        foreach ( $child_categories as $child_category ){
            $category_list[] = $child_category->term_id;
        }
        }
        
        $posts_args = array(
        'posts_per_page'   => 5,
        'cat'  => $catHindi,
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'suppress_filters' => true ,
        'offset' => 2
        );
        $tags = get_tags($args);
        
        $posts = new WP_Query ( $posts_args );
        if ( $posts->have_posts() ){
        while ( $posts->have_posts() ){
        $posts->the_post(); 
        $category_array = array();
        $post_categories = get_the_category ( get_the_ID() );
        
        if ( !empty ( $post_categories ) ){
           foreach ( $post_categories as $post_category ) {
              $category_array[] = $post_category->term_id;
            }
        }
        $result = array_diff( $category_array,  $category_list   ); 
        $image_arr = wp_get_attachment_image_src(get_post_thumbnail_id($post_array->ID), 'thumbnail');
        $image_url = $image_arr[0]; // $image_url is your URL.
        
        if ( empty ( $result ) ) { ?>
       <li>
        <div class="row">
          <div class="col-lg-4 col-4">
            <div class="img-center">
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <img src="<?php echo  $image_url; ?>" alt="img" />
              </a>
            </div>
          </div>
          <div class="col-lg-8 col-8 p-0">
            <div class="recent-blog-title">
              <span> <?php  	 echo get_the_date('M d, Y'); ?></span><br />
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
            </div>
          </div>
        </div>
      </li>
      <?php
        }
        }
        }
        wp_reset_postdata();
        ?>
    </ul>
  </div>
</div>