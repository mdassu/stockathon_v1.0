<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package stockathon-blog
 */

get_header();
$imageBlog = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
?>
<!-- Blog Banner -->
<!-- <section class="inner-page-banner">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div>
					<h1><?php echo the_title(); ?></h1>
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
					?>
					
				</div>
			</div>
		</div>
	</div>
</section> -->
<!-- End Blog Banner -->
<div class="container">
	<div class="row">
		<div class="col-xl-8 col-lg-8 col-md-6">
			<?php
			while ( have_posts() ) :
			the_post();

			// get_template_part( 'template-parts/content', get_post_type() );
			?>
<div class="blog-heading">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


				<div class="blog-heading">

          <?php if($imageBlog == ""){ ?>

        <?php  } else { ?>
          
          <div class=" align-items-center">
			<div class=" mx-auto text-center">
				<img class="img-fluid fullwidth" src="<?php echo $imageBlog[0] ?>" alt="">
			</div>
		</div>
				<?php }?>
		




		<!-- <?php //stockathon_blog_post_thumbnail(); ?> -->
		<div class="blog-detail-box detail-page-box">

			<?php
	
					if ( 'post' === get_post_type() ) :
						?>
			<div class="entry-meta">
				<?php
							// stockathon_blog_posted_on();
							// stockathon_blog_posted_by();
							?>
			</div><!-- .entry-meta -->
			<?php endif;
				?>
			<ul>
				<li class="text-capitalize"><i class="fas fa-user"></i> <?php the_author_meta( 'user_nicename'); ?> </li>
				<li><i class="far fa-calendar-alt"></i> <?php  	 echo get_the_date('M d, Y'); ?></li>
				<li class="cmtcount"><i class="fas fa-comment-dots"></i>
					<?php
							comments_popup_link( '0', '1 comment', '%', 'comments-link', '');
						?>
          </li>
				<li>
					<i class="fas fa-external-link-square-alt"></i>
					<?php
									$categories1 = get_the_category();
										if ( !empty( $categories1 ) ) {
												foreach( $categories1 as $category1 ) { ?>
					<?php  echo $category1->name ?>,
					<?php   } } ?>
				</li>
			</ul>
			<h5 class="text-capitalize"> 	<a href="<?php the_permalink() ?>"> <?php echo the_title(); ?> </a> </h5>
			<div class="program-detail">

				<?php 
							the_content(
								sprintf(
									wp_kses(
										/* translators: %s: Name of current post. Only visible to screen readers */
										__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'stockathon-blog' ),
										array(
											'span' => array(
												'class' => array(),
											),
										)
									),
									wp_kses_post( get_the_title() )
								)
							);
							?>
			</div>
			<?php
					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'stockathon-blog' ),
							'after'  => '</div>',
						)
					);
					?>
		</div>
	</article>
</div>
			<div class="blog-paging">
				<?php
						the_post_navigation(
							array(
								'screen_reader_text' => ' ', 
								'prev_text' => '' . esc_html__('', 'stockathon-blog' ) . '</span> <span class="nav-title">%title</span>',
								'next_text' => '' . esc_html__('', 'stockathon-blog' ) . '</span> <span class="nav-title">%title</span>',
							)
						);
					?>
			</div>
			<div id="comments" >
			<?php

				if ( comments_open() || get_comments_number() ) {
					comments_template(); 
				} else { ?>
			<div class="comment-box"> 

				<?php comments_template();  ?>
		</div>

			<?php } ?>
				</div>
					<?php
			endwhile; 
			?>
		</div>
		<!-- Get the sidebar hindi -->
		<div class="col-xl-4 col-lg-4 col-md-6">
				<div class="search-box">
          <?php get_search_form(); ?>
          </div>
          <div class="blog-categories">
          <?php get_sidebar(); ?>
          </div>
			</div>
	</div>
</div>

<!-- Get footer -->

<?php get_footer(); ?>