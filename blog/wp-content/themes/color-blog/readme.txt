﻿=== Color Blog ===

Contributors:       mysterythemes
Tags:               blog, portfolio, news, one-column, two-columns, left-sidebar, right-sidebar, custom-background, custom-colors, custom-header, custom-logo, custom-menu, editor-style, footer-widgets, rtl-language-support, theme-options, translation-ready
Requires at least:  4.7
Tested up to:       5.4
Requires PHP:       5.2.4
License:            GPLv3 or later
License URI:        http://www.gnu.org/licenses/gpl-3.0.html

== Description ==
Color Blog is a clean and colorful free blog style WordPress theme fit for writers and bloggers. With the help of live customizer option makes your site own and present your content in an attractive way. It comes up with an amazing creative blog layouts, fully RTL and translation ready, and also compatible with Gutenberg. The theme works perfectly with Elementor that helps to create a beautiful and unique website faster. As its name suggests, it added staggering variety of color and makes your site attractive and elegant. Demos ready for download: https://demo.mysterythemes.com/color-blog-landing/ and for support: https://mysterythemes.com/support/

Get free support at https://mysterythemes.com/support/forum/themes/free-themes/ and check the demo at https://demo.mysterythemes.com/color-blog-landing/

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


== Copyright ==

Color Blog WordPress Theme, Copyright 2019 Mystery Themes.
Color Blog is distributed under the terms of the GNU GPL

== Resource ==

	normalize.css, Copyright 2012-2016 Nicolas Gallagher and Jonathan Neal
    License: MIT Source: https://necolas.github.io/normalize.css/

    Theme name is based on Underscores http://underscores.me/,
    (C) 2012-2017 Automattic, Inc.
    License: GNU General Public License v2 or later

    Font Awesome by Dave Gandy
    Licenses: SIL OFL 1.1, MIT, CC BY 3.0
    Source: https://github.com/FortAwesome/Font-Awesome

    Sticky js, Copyright © 2014-2016, Anthony Garand
    License: GPL
    Source: https://github.com/garand/sticky

    Animate Css, Copyright © 2016 Daniel Eden
    License: MIT
    Source: https://github.com/daneden/animate.css

    Image Loaded Packaged by David DeSandro
    License: MIT
    Source: https://github.com/desandro/imagesloaded

    Masonry, Copyright © 2018, David DeSandro
    License: MIT License, http://www.opensource.org/licenses/MIT
    Source: http://masonry.desandro.com

    WOW, Copyright © 2016 Matthieu Aussaguel
    License: GPLv3
    Source: https://github.com/matthieua/WOW

    Lightslider, Copyright (c) 2015 Sachin N
    License: MIT License.
    Source: https://github.com/sachinchoolur/lightslider

    TGM-Plugin-Activation Copyright © 2011 by Thomas Griffin, Gary Jones, Juliette Reinders Folmer
    License : GPL
    Source: http://tgmpluginactivation.com/

    TRT Customizer Pro, Copyright 2016 by Justin Tadlock
    License: GPLv2 or later
    Source: https://github.com/justintadlock/trt-customizer-pro

    Screenshot Images

    Image for theme screenshot, Copyright Allef Vinicius
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/CIGMP8XYVH

    Image for theme screenshot, Copyright Jonas Svidras
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/6E5ZAMCKC4

    Image for theme screenshot, Copyright Elliott Chau
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/U3ESM8YMDM

    Image for theme screenshot, Copyright Burst
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/Y7VURRDV8Q

    Image for theme screenshot, Copyright Burst
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/HKTKMOVXNO

    Image for theme screenshot, Copyright Pete Johnson
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/08TH4N0XCS

    Image for theme screenshot, Copyright Greek Food - Ta Mystika
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/ACKZUHZIO1

    Image for theme screenshot, Copyright Aleksei Bakulin
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/OWO84DI5BR

    Image for theme screenshot, Copyright Greg Raines
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/TFGZBZ1EEV

    Image for theme screenshot, Copyright Lukáš Rychvalský
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/YCK0SQKB21

    Image for theme screenshot, Copyright Afta Putta Gunawan
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://stocksnap.io/photo/ZJBKGKNDIV

== Changelog ==

= 1.1.0 - November 24, 2020 =
    * Fixed - bugs at breadcrumb schema.

= 1.0.9 - October 01, 2020 =
    * Fixed - bugs at toogle button.

= 1.0.8 - August 13, 2020 =
    * Added - prefix at global variable.
    * Fixed - theme review css update.
    * Fixed - demo library at theme settings page.
    * Fixed - navigation key at primary menu section.
    * Fixed - continue reading link at single post page.
    * Removed - unused/commented code and files.
    * Updated - .pot file.

= 1.0.7 - May 22, 2020 =
    * Replaced  - welcome page with theme settings page.
    * Updated - .pot file.
    * Fixed - image size in single post page.

= 1.0.6 - April 23, 2020 =
    * Added - Added arrow icon on submenu item
    * Added - new plugin recommendation in tgm.
    * Updated - .pot file.

= 1.0.5 - April 06, 2020 =
    * Fixed - Some responsive bug fixed & some design adjust done.
    * Fixed - keyboard navigation.
    * Updated - .pot file.

= 1.0.4 - Nov 22, 2019 =
    * Default color for category color modify.
    * Code Refined.
    * Added TGM activation code.
    * Fixed bugs at welcome page.
    * Updated .pot file.

= 1.0.3 - Sept 5, 2019 =
    * Changes made in jetpack.php file.
    * Code Refined.
    * Updated .pot file.

= 1.0.2 - August 30, 2019 = 
    * Managed unstructured meta data.
    * Updated .pot file.

= 1.0.1 - August 11, 2019 = 
    * Added - Upsell button.
    * Added - Welcome Page.

= 1.0.0 - August 08, 2019 = 
	* Submit theme on wordpress.org trac.