#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Color Blog\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-01 09:32+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.2.1; wp-5.1.1"

#: 404.php:20
msgid "404"
msgstr ""

#: 404.php:20
msgid "error"
msgstr ""

#: 404.php:22
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:25
msgid "It looks like nothing was found at this location."
msgstr ""

#: 404.php:40 inc/customizer/mt-customizer-design-panel-options.php:244
msgid "You May Like"
msgstr ""

#. 1: title.
#: comments.php:36
#, php-format
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr ""

#. 1: comment count number, 2: title.
#: comments.php:42
#, php-format
msgctxt "comments title"
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:67
msgid "Comments are closed."
msgstr ""

#: functions.php:54
msgid "Top Header"
msgstr ""

#: functions.php:55
msgid "Primary"
msgstr ""

#: functions.php:56
msgid "Footer"
msgstr ""

#: header.php:41
msgid "Skip To Content"
msgstr ""

#: inc/mt-class-breadcrumbs.php:264
msgid "Browse:"
msgstr ""

#: inc/mt-class-breadcrumbs.php:265
msgctxt "breadcrumbs aria label"
msgid "Breadcrumbs"
msgstr ""

#: inc/mt-class-breadcrumbs.php:266
msgid "Home"
msgstr ""

#: inc/mt-class-breadcrumbs.php:267
msgid "404 Not Found"
msgstr ""

#: inc/mt-class-breadcrumbs.php:268
msgid "Archives"
msgstr ""

#. %s is the search query.
#: inc/mt-class-breadcrumbs.php:270
#, php-format
msgid "Search results for: %s"
msgstr ""

#. %s is the page number.
#: inc/mt-class-breadcrumbs.php:272
#, php-format
msgid "Page %s"
msgstr ""

#. %s is the page number.
#: inc/mt-class-breadcrumbs.php:274
#, php-format
msgid "Comment Page %s"
msgstr ""

#. Minute archive title. %s is the minute time format.
#: inc/mt-class-breadcrumbs.php:276
#, php-format
msgid "Minute %s"
msgstr ""

#. Weekly archive title. %s is the week date format.
#: inc/mt-class-breadcrumbs.php:278
#, php-format
msgid "Week %s"
msgstr ""

#: inc/mt-class-breadcrumbs.php:738
msgctxt "minute and hour archives time format"
msgid "g:i a"
msgstr ""

#: inc/mt-class-breadcrumbs.php:755
msgctxt "minute archives time format"
msgid "i"
msgstr ""

#: inc/mt-class-breadcrumbs.php:772
msgctxt "hour archives time format"
msgid "g a"
msgstr ""

#: inc/mt-class-breadcrumbs.php:788 inc/mt-class-breadcrumbs.php:817
#: inc/mt-class-breadcrumbs.php:844 inc/mt-class-breadcrumbs.php:871
#: inc/mt-class-breadcrumbs.php:1211
msgctxt "yearly archives date format"
msgid "Y"
msgstr ""

#: inc/mt-class-breadcrumbs.php:789 inc/mt-class-breadcrumbs.php:845
#: inc/mt-class-breadcrumbs.php:1215
msgctxt "monthly archives date format"
msgid "F"
msgstr ""

#: inc/mt-class-breadcrumbs.php:790 inc/mt-class-breadcrumbs.php:1219
msgctxt "daily archives date format"
msgid "j"
msgstr ""

#: inc/mt-class-breadcrumbs.php:818
msgctxt "weekly archives date format"
msgid "W"
msgstr ""

#. If there are characters in your language that are not supported
#. byJosefin Sans  translate this to 'off'. Do not translate into your own language.
#: inc/template-functions.php:89
msgctxt "Josefin Sans font: on or off"
msgid "on"
msgstr ""

#. If there are characters in your language that are not supported
#. by Poppins, translate this to 'off'. Do not translate into your own language.
#: inc/template-functions.php:97
msgctxt "Poppins font: on or off"
msgid "on"
msgstr ""

#: inc/template-functions.php:305
msgid "Select Category"
msgstr ""

#. %s: post title
#: inc/template-tags.php:70
#, php-format
msgid "Leave a Comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#. used between list items, there is a space after the comma
#: inc/template-tags.php:97 inc/template-tags.php:142
msgctxt "list item separator"
msgid ", "
msgstr ""

#. 1: list of tags.
#: inc/template-tags.php:100 inc/template-tags.php:145
#, php-format
msgid "Tagged %1$s"
msgstr ""

#: inc/template-tags.php:106
#: inc/customizer/mt-customizer-design-panel-options.php:87
msgid "Discover"
msgstr ""

#. %s: Name of current post. Only visible to screen readers
#: inc/template-tags.php:116 inc/template-tags.php:153
#: template-parts/content-page.php:31
#, php-format
msgid "Edit <span class=\"screen-reader-text\">%s</span>"
msgstr ""

#: template-parts/content-none.php:15
msgid "Nothing Found"
msgstr ""

#. 1: link to WP admin new post page.
#: template-parts/content-none.php:24
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:35
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: template-parts/content-none.php:40
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: template-parts/content-page.php:18 template-parts/content-single.php:53
msgid "Pages:"
msgstr ""

#. %s: Name of current post. Only visible to screen readers
#: template-parts/content-search.php:43 template-parts/content-single.php:43
#: template-parts/content.php:56
#, php-format
msgid "Continue reading<span class=\"screen-reader-text\"> \"%s\"</span>"
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:22
msgid "Social Icons"
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:55
msgid "Social Media"
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:59
msgid "Social Media Icons"
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:60
msgid "Add Icon"
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:65
msgid "Social Icon"
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:66
msgid "Choose social media icon."
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:70
msgid "Social Link URL"
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:71
msgid "Enter social media url."
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:83
msgid "Breadcrumbs"
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:106
msgid "Enable Breadcrumbs"
msgstr ""

#: inc/customizer/mt-customizer-additional-panel-options.php:120
msgid "Categories Color"
msgstr ""

#: inc/customizer/mt-customizer-custom-classes.php:305
msgid "&mdash; Select a page &mdash;"
msgstr ""

#: inc/customizer/mt-customizer-custom-classes.php:334
#: inc/widgets/mt-widget-fields.php:128
msgid "Select Image"
msgstr ""

#: inc/customizer/mt-customizer-custom-classes.php:335
#: inc/widgets/mt-widget-fields.php:127
msgid "Remove"
msgstr ""

#: inc/customizer/mt-customizer-custom-classes.php:340
#: inc/widgets/mt-widget-fields.php:120
msgid "No image selected"
msgstr ""

#: inc/customizer/mt-customizer-custom-classes.php:353
msgid "Delete"
msgstr ""

#: inc/customizer/mt-customizer-custom-classes.php:354
msgid "Close"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:23
msgid "Archive Settings"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:43
msgid "Archive/Blog Sidebar Layout"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:69
msgid "Archive/Blog Style"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:95
msgid "Read More Button"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:107
msgid "Post Settings"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:126
msgid "Posts Sidebar Layout"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:154
msgid "Enable Related Posts"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:168
msgid "Page Settings"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:188
msgid "Pages Sidebar Layout"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:212
msgid "404 Page Settings"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:230
msgid "Enable Latest Posts"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:252
msgid "Section Title"
msgstr ""

#: inc/customizer/mt-customizer-design-panel-options.php:273
msgid "Post count"
msgstr ""

#: inc/customizer/mt-customizer-footer-panel-options.php:22
msgid "Footer Widget Area"
msgstr ""

#: inc/customizer/mt-customizer-footer-panel-options.php:44
msgid "Enable Footer Widget Area"
msgstr ""

#: inc/customizer/mt-customizer-footer-panel-options.php:64
msgid "Widget Area Layout"
msgstr ""

#: inc/customizer/mt-customizer-footer-panel-options.php:65
msgid "Choose widget layout from available layouts"
msgstr ""

#: inc/customizer/mt-customizer-footer-panel-options.php:86
msgid "Bottom Footer"
msgstr ""

#: inc/customizer/mt-customizer-footer-panel-options.php:108
msgid "Enable Footer Menu"
msgstr ""

#. Name of the theme
#: inc/customizer/mt-customizer-footer-panel-options.php:123
#: inc/hooks/mt-custom-hooks.php:554
#: inc/theme-settings/mt-theme-settings.php:499
msgid "Color Blog"
msgstr ""

#: inc/customizer/mt-customizer-footer-panel-options.php:131
msgid "Copyright Text"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:27
msgid "Slider Settings"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:46
msgid "Enable Slider Section"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:69
msgid "Slider category"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:70
msgid "Choose default post category"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:89
msgid "Featured Posts Settings"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:108
msgid "Enable Featured Posts Section"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:123
#: inc/customizer/mt-customizer-front-panel-options.php:131
#: inc/hooks/mt-custom-hooks.php:443
msgid "Featured News"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:152
msgid "Featured Post Order"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:156
msgid "Latest Posts"
msgstr ""

#: inc/customizer/mt-customizer-front-panel-options.php:157
msgid "Random Posts"
msgstr ""

#: inc/customizer/mt-customizer-general-panel-options.php:26
msgid "Site Settings"
msgstr ""

#: inc/customizer/mt-customizer-general-panel-options.php:46
msgid "Enable Preloader"
msgstr ""

#: inc/customizer/mt-customizer-general-panel-options.php:69
msgid "Enable Wow Animation"
msgstr ""

#: inc/customizer/mt-customizer-general-panel-options.php:91
msgid "Site Layout"
msgstr ""

#: inc/customizer/mt-customizer-general-panel-options.php:92
msgid "Choose site layout from available layouts"
msgstr ""

#: inc/customizer/mt-customizer-general-panel-options.php:118
msgid "Primary Color"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:27
msgid "Top Header Settings"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:46
msgid "Enable Top Header"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:47
msgid "Show/Hide top header section."
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:70
msgid "Enable Trending Section"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:71
msgid "Trending section shows the popular tags."
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:95
msgid "Add Icon Before Tag"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:96
msgid "Show/Hide Hash Icon before tag."
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:110
#: inc/hooks/mt-top-header-hooks.php:46
msgid "Trending Now"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:118
msgid "Trending Label"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:140
msgid "Tags Orderby"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:145
msgid "Default"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:146
msgid "Count"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:167
msgid "Tags Count"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:184
msgid "Extra Options"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:203
msgid "Enable Sticky Menu"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:226
msgid "Enable Social Icons"
msgstr ""

#: inc/customizer/mt-customizer-header-panel-options.php:249
msgid "Enable Search Icon"
msgstr ""

#: inc/customizer/mt-customizer-panels.php:24
msgid "General Settings"
msgstr ""

#: inc/customizer/mt-customizer-panels.php:36
msgid "Header Settings"
msgstr ""

#: inc/customizer/mt-customizer-panels.php:48
msgid "Front Sections"
msgstr ""

#: inc/customizer/mt-customizer-panels.php:60
msgid "Design Settings"
msgstr ""

#: inc/customizer/mt-customizer-panels.php:72
msgid "Additional Features"
msgstr ""

#: inc/customizer/mt-customizer-panels.php:84
msgid "Footer Settings"
msgstr ""

#: inc/customizer/mt-customizer.php:32
msgid "Header Image for only Innerpages"
msgstr ""

#: inc/customizer/mt-customizer.php:67
#: inc/theme-settings/mt-theme-settings.php:500
msgid "Color Blog Pro"
msgstr ""

#: inc/customizer/mt-customizer.php:68
#: inc/theme-settings/mt-theme-settings.php:391
msgid "Buy Now"
msgstr ""

#: inc/hooks/mt-custom-hooks.php:79
msgid "Menu"
msgstr ""

#: inc/hooks/mt-custom-hooks.php:116
msgid "Follow Us: "
msgstr ""

#: inc/hooks/mt-custom-hooks.php:137
msgid "Search"
msgstr ""

#: inc/hooks/mt-custom-hooks.php:273
msgid "Back To Top"
msgstr ""

#: inc/hooks/mt-custom-hooks.php:314
#, php-format
msgid "Search Results for: %s"
msgstr ""

#. 1: Theme name, 2: Theme author.
#: inc/hooks/mt-custom-hooks.php:561
#, php-format
msgid "Theme: %1$s by %2$s."
msgstr ""

#: inc/metaboxes/mt-post-sidebar-meta.php:18
#: inc/metaboxes/mt-post-sidebar-meta.php:27
msgid "Sidebar Layout"
msgstr ""

#: inc/metaboxes/mt-post-sidebar-meta.php:44
msgid "Default Sidebar"
msgstr ""

#: inc/metaboxes/mt-post-sidebar-meta.php:50
msgid "Left sidebar"
msgstr ""

#: inc/metaboxes/mt-post-sidebar-meta.php:56
msgid "Right sidebar"
msgstr ""

#: inc/metaboxes/mt-post-sidebar-meta.php:62
msgid "No sidebar Full width"
msgstr ""

#: inc/metaboxes/mt-post-sidebar-meta.php:68
msgid "No sidebar Content Centered"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:327
msgid "Install Required Plugins"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:328
msgid "Install Plugins"
msgstr ""

#. %s: plugin name.
#: inc/tgm/class-tgm-plugin-activation.php:330
#, php-format
msgid "Installing Plugin: %s"
msgstr ""

#. %s: plugin name.
#: inc/tgm/class-tgm-plugin-activation.php:332
#, php-format
msgid "Updating Plugin: %s"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:333
msgid "Something went wrong with the plugin API."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:334
#, php-format
msgid "This theme requires the following plugin: %1$s."
msgid_plural "This theme requires the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:340
#, php-format
msgid "This theme recommends the following plugin: %1$s."
msgid_plural "This theme recommends the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:346
#, php-format
msgid ""
"The following plugin needs to be updated to its latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgid_plural ""
"The following plugins need to be updated to their latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:352
#, php-format
msgid "There is an update available for: %1$s."
msgid_plural "There are updates available for the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:358
#, php-format
msgid "The following required plugin is currently inactive: %1$s."
msgid_plural "The following required plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:364
#, php-format
msgid "The following recommended plugin is currently inactive: %1$s."
msgid_plural "The following recommended plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:370
msgid "Begin installing plugin"
msgid_plural "Begin installing plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:375
msgid "Begin updating plugin"
msgid_plural "Begin updating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:380
msgid "Begin activating plugin"
msgid_plural "Begin activating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:385
msgid "Return to Required Plugins Installer"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:386
#: inc/tgm/class-tgm-plugin-activation.php:827
#: inc/tgm/class-tgm-plugin-activation.php:2533
#: inc/tgm/class-tgm-plugin-activation.php:3580
msgid "Return to the Dashboard"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:387
#: inc/tgm/class-tgm-plugin-activation.php:3159
msgid "Plugin activated successfully."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:388
#: inc/tgm/class-tgm-plugin-activation.php:2952
msgid "The following plugin was activated successfully:"
msgid_plural "The following plugins were activated successfully:"
msgstr[0] ""
msgstr[1] ""

#. 1: plugin name.
#: inc/tgm/class-tgm-plugin-activation.php:390
#, php-format
msgid "No action taken. Plugin %1$s was already active."
msgstr ""

#. 1: plugin name.
#: inc/tgm/class-tgm-plugin-activation.php:392
#, php-format
msgid ""
"Plugin not activated. A higher version of %s is needed for this theme. "
"Please update the plugin."
msgstr ""

#. 1: dashboard link.
#: inc/tgm/class-tgm-plugin-activation.php:394
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:395
msgid "Dismiss this notice"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:396
msgid ""
"There are one or more required or recommended plugins to install, update or "
"activate."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:397
msgid "Please contact the administrator of this site for help."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:522
msgid "This plugin needs to be updated to be compatible with your theme."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:523
msgid "Update Required"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:934
msgid ""
"The remote plugin package does not contain a folder with the desired slug "
"and renaming did not work."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:934
#: inc/tgm/class-tgm-plugin-activation.php:937
msgid ""
"Please contact the plugin provider and ask them to package their plugin "
"according to the WordPress guidelines."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:937
msgid ""
"The remote plugin package consists of more than one file, but the files are "
"not packaged in a folder."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:1121
#: inc/tgm/class-tgm-plugin-activation.php:2948
msgctxt "plugin A *and* plugin B"
msgid "and"
msgstr ""

#. %s: version number
#: inc/tgm/class-tgm-plugin-activation.php:1982
#, php-format
msgid "TGMPA v%s"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2273
msgid "Required"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2276
msgid "Recommended"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2292
msgid "WordPress Repository"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2295
msgid "External Source"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2298
msgid "Pre-Packaged"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2315
msgid "Not Installed"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2319
msgid "Installed But Not Activated"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2321
msgid "Active"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2327
msgid "Required Update not Available"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2330
msgid "Requires Update"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2333
msgid "Update recommended"
msgstr ""

#. 1: install status, 2: update status
#: inc/tgm/class-tgm-plugin-activation.php:2342
#, php-format
msgctxt "Install/Update Status"
msgid "%1$s, %2$s"
msgstr ""

#. 1: number of plugins.
#: inc/tgm/class-tgm-plugin-activation.php:2388
#, php-format
msgctxt "plugins"
msgid "All <span class=\"count\">(%s)</span>"
msgid_plural "All <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#. 1: number of plugins.
#: inc/tgm/class-tgm-plugin-activation.php:2392
#, php-format
msgid "To Install <span class=\"count\">(%s)</span>"
msgid_plural "To Install <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#. 1: number of plugins.
#: inc/tgm/class-tgm-plugin-activation.php:2396
#, php-format
msgid "Update Available <span class=\"count\">(%s)</span>"
msgid_plural "Update Available <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#. 1: number of plugins.
#: inc/tgm/class-tgm-plugin-activation.php:2400
#, php-format
msgid "To Activate <span class=\"count\">(%s)</span>"
msgid_plural "To Activate <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/tgm/class-tgm-plugin-activation.php:2482
msgctxt "as in: \"version nr unknown\""
msgid "unknown"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2490
msgid "Installed version:"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2498
msgid "Minimum required version:"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2510
msgid "Available version:"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2533
msgid "No plugins to install, update or activate."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2547
msgid "Plugin"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2548
msgid "Source"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2549
msgid "Type"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2553
msgid "Version"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2554
msgid "Status"
msgstr ""

#. %2$s: plugin name in screen reader markup
#: inc/tgm/class-tgm-plugin-activation.php:2603
#, php-format
msgid "Install %2$s"
msgstr ""

#. %2$s: plugin name in screen reader markup
#: inc/tgm/class-tgm-plugin-activation.php:2608
#, php-format
msgid "Update %2$s"
msgstr ""

#. %2$s: plugin name in screen reader markup
#: inc/tgm/class-tgm-plugin-activation.php:2614
#, php-format
msgid "Activate %2$s"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2684
msgid "Upgrade message from the plugin author:"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2717
msgid "Install"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2723
msgid "Update"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2726
msgid "Activate"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2757
msgid "No plugins were selected to be installed. No action taken."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2759
msgid "No plugins were selected to be updated. No action taken."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2800
msgid "No plugins are available to be installed at this time."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2802
msgid "No plugins are available to be updated at this time."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2908
msgid "No plugins were selected to be activated. No action taken."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:2934
msgid "No plugins are available to be activated at this time."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:3158
msgid "Plugin activation failed."
msgstr ""

#. 1: plugin name, 2: action number 3: total number of actions.
#: inc/tgm/class-tgm-plugin-activation.php:3498
#, php-format
msgid "Updating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#. 1: plugin name, 2: error message.
#: inc/tgm/class-tgm-plugin-activation.php:3501
#, php-format
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#. 1: plugin name.
#: inc/tgm/class-tgm-plugin-activation.php:3503
#, php-format
msgid "The installation of %1$s failed."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:3507
msgid ""
"The installation and activation process is starting. This process may take a "
"while on some hosts, so please be patient."
msgstr ""

#. 1: plugin name.
#: inc/tgm/class-tgm-plugin-activation.php:3509
#, php-format
msgid "%1$s installed and activated successfully."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:3509
#: inc/tgm/class-tgm-plugin-activation.php:3517
msgid "Show Details"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:3509
#: inc/tgm/class-tgm-plugin-activation.php:3517
msgid "Hide Details"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:3510
msgid "All installations and activations have been completed."
msgstr ""

#. 1: plugin name, 2: action number 3: total number of actions.
#: inc/tgm/class-tgm-plugin-activation.php:3512
#, php-format
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:3515
msgid ""
"The installation process is starting. This process may take a while on some "
"hosts, so please be patient."
msgstr ""

#. 1: plugin name.
#: inc/tgm/class-tgm-plugin-activation.php:3517
#, php-format
msgid "%1$s installed successfully."
msgstr ""

#: inc/tgm/class-tgm-plugin-activation.php:3518
msgid "All installations have been completed."
msgstr ""

#. 1: plugin name, 2: action number 3: total number of actions.
#: inc/tgm/class-tgm-plugin-activation.php:3520
#, php-format
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/tgm/mt-recommend-plugins.php:64
msgid "WP Blog Post Layouts"
msgstr ""

#: inc/tgm/mt-recommend-plugins.php:73
msgid "WP Magazine Modules Lite"
msgstr ""

#: inc/tgm/mt-recommend-plugins.php:82
msgid "Contact Form, Drag and Drop Form Builder for WordPress – Everest Forms"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:44
msgid "Settings"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:135
msgid "Action failed. Please refresh the page and retry."
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:139
msgid "Cheat in &#8217; huh?"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:156
msgid "Dismiss this notice."
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:158
#, php-format
msgid "Welcome to %s"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:160
#, php-format
msgid ""
"Welcome! Thank you for choosing %1$s ! To fully take advantage of the best "
"our theme can offer please make sure you visit our %2$s theme settings page "
"%3$s."
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:163
#, php-format
msgid ""
"Clicking get started will process to installation of %1$s Mystery Themes "
"Demo Importer %2$s Plugin in your dashboard. After success it will redirect "
"to the theme settings page."
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:166
msgid "Done!"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:166
msgid "Processing"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:167
#, php-format
msgid "Get started with %1$s"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:192
#, php-format
msgid "Version: %1$s"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:199
msgid "Upgrade to Premium Version"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:200
msgid "Upgrade to pro version for additional features and better supports."
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:202
msgid "Unlock Features With Pro"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:209
msgid "Get Started"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:213
msgid "Demos"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:217
msgid "Free Vs Pro"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:221
msgid "Changelog"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:260
msgid "Here are some useful links for you to get started"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:263
msgid "Next Steps"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:266
msgid "Set site logo"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:269
msgid "Setup site layout"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:272
msgid "Manage header section"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:275
msgid "Single page sidebar layouts"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:278
msgid "Manage Social Icons"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:281
msgid "Manage footer widget area"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:284
msgid "Manage menus"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:287
msgid "Manage widgets"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:293
msgid "More Actions"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:296
msgid "Documentation"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:299
msgid "Premium version"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:302
msgid "Need theme support?"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:305
msgid "Review theme"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:308
msgid "WP Tutorials"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:318
msgid "Return to Updates"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:318
msgid "Return to Dashboard &rarr; Updates"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:321
msgid "Go to Dashboard &rarr; Home"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:321
msgid "Go to Dashboard"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:355
msgid "No demos are configured for this theme, please contact the theme author"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:381
#, php-format
msgid "Imported %1$s"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:382
msgid "Imported"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:390
msgid "Purchase Now"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:396
msgid "Click to import demo"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:398
msgid "Demo importer plugin is not installed or activated"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:401
#, php-format
msgid "Import %1$s"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:402
msgid "Import"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:409
msgid "View Demo"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:442
msgid "View changelog below:"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:494
msgid "Upgrade to PRO version for more exciting features."
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:498
msgid "Features"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:505
msgid "Price"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:506
msgid "Free"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:507
msgid "$59.99"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:510
msgid "Import Demo Data"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:515
msgid "Pre Loaders Layouts"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:520
msgid "Header Layouts"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:521
msgid "1"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:522
msgid "4"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:525
msgid "Multiple Layouts"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:530
msgid "Google Fonts"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:531
msgid "2"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:532
msgid "600+"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:535
msgid "WordPress Page Builder Compatible"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:540
msgid "Custom 404 Page"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:545
msgid "Typography Options"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:550
msgid "Footer Layout Options"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:555
msgid "WooCommerce Plugin Compatible"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:560
msgid "GDPR Compatible"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:568
msgid "Buy Pro"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:621
#, php-format
msgid ""
"Howdy, %1$s! It seems that you have been using this theme for more than 15 "
"days. We hope you are happy with everything that the theme has to offer. If "
"you can spare a minute, please help us by leaving a 5-star review on "
"WordPress.org.  By spreading the love, we can continue to develop new "
"amazing features in the future, for free!"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:632
msgid "Sure"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:637
msgid "Maybe later"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:642
msgid "I already did"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:647
msgid "Got theme support question?"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:728
#, php-format
msgid ""
"If you like <strong>%1$s</strong> please leave us a %2$s rating. A huge "
"thank you from <strong>Mystery Themes</strong> in advance &#128515!"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:746
msgid "You can import available demos now!"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:750
msgid "Mystery Themes Demo Importer Plugin is not installed!"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:751
msgid "Installing & Activating"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:751
msgid "Installed & Activated"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:752
msgid "Install and Activate"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:757
msgid "Mystery Themes Demo Importer Plugin is installed but not activated!"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:758
msgid "Activating"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:758
msgid "Activated"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:759
msgid "Activate Now"
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:793
msgid "Plugin Successfully Activated."
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:811
msgid "Sorry, you are not allowed to install plugins on this site."
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:856
msgid "Unable to connect to the filesystem. Please confirm your credentials."
msgstr ""

#: inc/theme-settings/mt-theme-settings.php:874
msgid "Plugin installed successfully"
msgstr ""

#: inc/widgets/mt-author-info.php:20
msgid "Select the user to display the author info."
msgstr ""

#: inc/widgets/mt-author-info.php:23
msgid "MT: Author Info"
msgstr ""

#: inc/widgets/mt-author-info.php:35 inc/widgets/mt-latest-posts.php:36
#: inc/widgets/mt-social-media.php:36
msgid "Widget title"
msgstr ""

#: inc/widgets/mt-author-info.php:41
msgid "User Name"
msgstr ""

#: inc/widgets/mt-author-info.php:47
msgid "Select Author"
msgstr ""

#: inc/widgets/mt-author-info.php:54
msgid "Author Image"
msgstr ""

#: inc/widgets/mt-latest-posts.php:20
msgid "A widget to display the latest posts with thumbnail."
msgstr ""

#: inc/widgets/mt-latest-posts.php:23
msgid "MT: Latest Posts"
msgstr ""

#: inc/widgets/mt-latest-posts.php:42
msgid "Post Order"
msgstr ""

#: inc/widgets/mt-latest-posts.php:46
msgid "Default Order"
msgstr ""

#: inc/widgets/mt-latest-posts.php:47
msgid "Random Order"
msgstr ""

#: inc/widgets/mt-latest-posts.php:53
msgid "Post Count"
msgstr ""

#: inc/widgets/mt-social-media.php:20
msgid "A widget shows the social media icons."
msgstr ""

#: inc/widgets/mt-social-media.php:23
msgid "MT: Social Media"
msgstr ""

#: inc/widgets/mt-widget-fields.php:71
msgid "- - Select User - -"
msgstr ""

#: inc/widgets/mt-widget-functions.php:22
msgid "Sidebar"
msgstr ""

#: inc/widgets/mt-widget-functions.php:24
msgid "Add widgets here."
msgstr ""

#: inc/widgets/mt-widget-functions.php:38
msgid "Header Ads Section"
msgstr ""

#: inc/widgets/mt-widget-functions.php:40
msgid "Add MT: Ads Banner widgets here."
msgstr ""

#: inc/widgets/mt-widget-functions.php:55
#, php-format
msgid "Footer %d"
msgstr ""

#: inc/widgets/mt-widget-functions.php:57
msgid "Added widgets are display at Footer Widget Area."
msgstr ""

#: template-parts/author/post-author-box.php:38
msgid "Website"
msgstr ""

#: template-parts/related/related-posts.php:21
msgid "Related Posts"
msgstr ""

#. Description of the theme
msgid ""
"Color Blog is the perfect WordPress theme entirely based on any kind of blog "
"such as travel blogs, food blogs, personal blogs and many more. Build an "
"amazing website with the core features and resourceful design that let you "
"feel the awesome experience. Works perfectly with Elementor and Visual "
"Composer that will help you create an enchanting website. It has a "
"minimalist design, responsive and looks great on any kind of devices. Color "
"Blog is Compatible with Gutenberg and GDPR, RTL and Translation ready, "
"WooCommerce Compatible, SEO-Friendly and Compatible with all others popular "
"plugins. Demos ready for download: https://demo.mysterythemes.com/color-blog-"
"landing/ and for support: https://mysterythemes.com/support/"
msgstr ""

#. URI of the theme
msgid "https://mysterythemes.com/wp-themes/color-blog"
msgstr ""

#. Author of the theme
msgid "Mystery Themes"
msgstr ""

#. Author URI of the theme
msgid "https://mysterythemes.com"
msgstr ""
