const withSass = require('@zeit/next-sass');
const withCSS = require("@zeit/next-css");
module.exports = withCSS(withSass({
  webpack(config, options) {
    config.module.rules.push({
      test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000
        }
      }
    });

    return config;
  },
  env: {
    GET_API_URL: 'https://services.stockathon.com/api/StockGet/',
    POST_API_URL: 'https://services.stockathon.com/api/StockPost/',
    IMG_URL: 'https://services.stockathon.com/',
    FACEBOOK_LOGIN_CLIENT_ID: '723001804996749',
    GOOGLE_LOGIN_CLIENT_ID: '997770512657-6acedvutd6gkiifvn61d3oomeotljenb.apps.googleusercontent.com',
  },
  // env: {
  //   GET_API_URL: 'http://65.0.219.217/api/StockGet/',
  //   POST_API_URL: 'http://65.0.219.217/api/StockPost/',
  //   IMG_URL: 'http://65.0.219.217/',
  //   FACEBOOK_LOGIN_CLIENT_ID: '377741300232516',
  //   GOOGLE_LOGIN_CLIENT_ID: '997770512657-6acedvutd6gkiifvn61d3oomeotljenb.apps.googleusercontent.com',
  // },
}));