import React from 'react';
import { httpGet } from '../_helper/ApiBase';
var data = null;
export const CommonService = {
  getConfiguration,
  getHomeNews,
  getMetaData
};

function getConfiguration(cb) {

  httpGet('getconfiguration', (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.data
        }
        // globalConfiguration.next();
      } else {
        var res = {
          status: false,
          message: response.message
        }
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

function getHomeNews(cb) {

  httpGet('market_news', (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.data
        }
        // globalConfiguration.next();
      } else {
        var res = {
          status: false,
          message: response.message
        }
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

function getMetaData(cb) {

  httpGet('seoList', (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.data
        }
        // globalConfiguration.next();
      } else {
        var res = {
          status: false,
          message: response.message
        }
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}
