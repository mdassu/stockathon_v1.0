import Cookies from 'js-cookie';
import { BehaviorSubject } from 'rxjs';
import { httpPostTemp, httpUpoadPost } from '_helper/ApiBase';
import { encrypt, decrypt } from '_helper/EncrDecrypt';
import Router from 'next/router';

var data = null;
if (Cookies.get('_user')) {
  data = decrypt(Cookies.get('_user'));
}
const currentUserSubject = new BehaviorSubject(data);

export const AuthenticationService = {
  signUp,
  setCurrentUserSubject,
  logout,
  currentUserSubject,
  purchasePlan,
  currentUser: currentUserSubject.asObservable(),
  get currentUserValue() { return currentUserSubject.value },
  get isLogged() {
    if (this.currentUserValue && this.currentUserValue != null) {
      return true;
    } else {
      return false;
    }
  }
};

function setCurrentUserSubject(userData) {
  currentUserSubject.next(userData);
}

function logout() {
  // remove user from local storage to log user out
  Cookies.remove('_user');
  Cookies.remove('_accessToken');
  currentUserSubject.next(null);
  Router.push('/');
}

// function clearCookies() {
//   Cookies.remove('_pack');
//   Cookies.remove('_match');
//   Cookies.remove('_ctData');
//   Cookies.remove('_pId');
//   Cookies.remove('_amount');
//   Cookies.remove('_mobData');
//   Cookies.remove('_wR');
// }

// signup api
function signUp(params, cb) {

  httpPostTemp('signUp', params, (response) => {
    try {
      if (Object.keys(response).length != 0) {
        if (response.result == 1) {
          var data;
          var res;
          console.log(response);
          if (response.data) {
            data = response.data;
            Cookies.set('_accessToken', data['token']);
          } else {
            data = {}
          }
          res = {
            status: true,
            data: data
          };
        } else {
          res = {
            status: false,
            message: response.message
          };
        }
        cb(res);
      }
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

// signup api
function purchasePlan(params, cb) {

  httpPostTemp('saveUserOrderDetails', params, (response) => {
    try {
      if (response.result == 1) {
        var data;
        var res;
        if (response.data) {
          data = response.data;
          Cookies.set('_accessToken', data['token']);
        } else {
          data = {}
        }
        res = {
          status: true,
          data: data
        };
      } else {
        res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}