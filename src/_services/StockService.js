import Cookies from 'js-cookie';
import { httpPost, httpPostTemp } from '_helper/ApiBase';

export const StockService = {
  getStockList,
  getPopularStock,
  getChartData,
  getFinancialYear,
  getStockDetails,
  getPeersDetails,
  getPressDetails,
  savePeerColumns,
  submitRequest,
  getStockNews
};

function getStockList(params, cb) {

  httpPostTemp('getStockList', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getPopularStock(params, cb) {

  httpPostTemp('getPopularStockList', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getChartData(params, cb) {

  httpPostTemp('getCandleDaily', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getFinancialYear(params, cb) {

  httpPostTemp('getSECFilings', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getPressDetails(params, cb) {

  httpPostTemp('pressRelease', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getStockDetails(params, cb) {

  httpPostTemp('getStockData', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data'],
          stockAvailable: response['stockAvailable']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

export function getStockMetaDetails(params) {
  httpPostTemp('getStockMetaData', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
        return response['data'];
      } else {
        var res = {
          status: false,
          data: {}
        };
        return {};
      }
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getPeersDetails(params, cb) {

  httpPostTemp('getPeersData', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function savePeerColumns(params, cb) {

  httpPostTemp('savePeerColumn', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function submitRequest(params, cb) {

  httpPostTemp('submitStockReq', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          message: response['message']
        };
      } else {
        var res = {
          status: false,
          message: response['message']
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getStockNews(params, cb) {

  httpPostTemp('company_news', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          message: response['message']
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}