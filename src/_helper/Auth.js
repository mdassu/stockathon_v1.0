import React, { createContext, useState, useContext, useEffect } from 'react'
import Cookies from 'js-cookie'
import Router, { useRouter } from 'next/router';
import { decrypt } from '_helper/EncrDecrypt';

//api here is an axios instance which has the baseURL set according to the env.
// import api from '../services/Api';
import Loader from 'components/Common/Loader';
import { AuthenticationService } from '_services/AuthenticationService';


const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {

  const [user, setUser] = useState()
  const [isLoading, setLoading] = useState(true)

  useEffect(() => {
    async function loadUserFromCookies() {
      const token = Cookies.get('_accessToken')
      console.log(token);
      if (token) {
        console.log("Got a token in the cookies, let's see if it is valid")
        // api.defaults.headers.Authorization = `Bearer ${token}`
        const user = decrypt(Cookies.get('_user'));
        if (user) setUser(user);
        console.log(!!user);
      }
      setLoading(false)
    }
    loadUserFromCookies()
  }, [])

  // const login = async (email, password) => {
  //   const { data: token } = await api.post('auth/login', { email, password })
  //   if (token) {
  //     console.log("Got token")
  //     Cookies.set('token', token, { expires: 60 })
  //     api.defaults.headers.Authorization = `Bearer ${token.token}`
  //     const { data: user } = await api.get('users/me')
  //     setUser(user)
  //     console.log("Got user", user)
  //   }
  // }

  const logout = (email, password) => {
    Cookies.remove('_user');
    Cookies.remove('_accessToken');
    AuthenticationService.currentUserSubject.next(null);
    // setUser(null)
    // delete api.defaults.headers.Authorization
    Router.push('/');
  }


  return (
    <AuthContext.Provider value={{ isAuthenticated: !!user, user, isLoading, logout }}>
      {children}
    </AuthContext.Provider>
  )
}



export const useAuth = () => useContext(AuthContext);

export const ProtectRoute = ({ children }) => {
  const router = useRouter();
  const { isAuthenticated, isLoading } = useAuth();
  if (isLoading) {
    return <Loader />;
  } else if (isAuthenticated && router.pathname == '/login') {
    return <GoToHome />
  } else {
    return children;
  }
};

export const GoToHome = ({ children }) => {
  const router = useRouter();
  router.push('/')
  return (
    <div className="h-100 fullloader">
      <img className="loader-img" src="/images/loader.gif" alt='loader' />
    </div>
  )
}