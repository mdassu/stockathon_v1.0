import Head from 'next/head'
import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Layout from "../components/Layout/Layout";
import Cookies from 'js-cookie';
import { decrypt } from '_helper/EncrDecrypt';
import { getMetaData } from '_helper/MetaData';

class Terms extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metaData: {}
    }
  }
  componentDidMount() {

  }

  render() {
    const { metaData } = this.props;
    return (
      <Layout title={metaData && metaData != '' ? metaData['page_title'] : ''} metaKeywords={metaData && metaData != '' ? metaData['page_keyword'] : ''} metaDesc={metaData && metaData != '' ? metaData['page_description'] : ''}>
        {/* Terms Start */}
        <Container>
          {/* Row Start */}
          <Row>
            <Col md="12">
              <div className="terms-content">
                <h1>Terms of Service</h1>
                <h4>Welcome to Stockathon</h4>
                <p>Thanks for using our products and services ("Services").
                The Services are provided by Stockathon through its network
                of websites and domains at stockathon.in ("Site").
                By using our Services, you are agreeing to these terms. Please read them carefully.
                </p>
                <h4>Terms</h4>
                <p>By accessing the Site, you are agreeing to be bound by our terms of service, all applicable laws
                and regulations, and agree that you are responsible for compliance with any applicable local laws.
                If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this
                website are protected by applicable copyright and trademark law.
                </p>
                <h4>Service License</h4>
                <p>A. Permission is granted to use the materials (information or software) on Site for personal, non-commercial transitory viewing only.
                This is the grant of a license, not a transfer of title, and under this license you may not:
                </p>
                <ul>
                  <li>Modify or copy the materials;</li>
                  <li>Use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>
                  <li>Attempt to decompile or reverse engineer any software contained on Site;</li>
                  <li>Remove any copyright or other proprietary notations from the materials;</li>
                  <li>Transfer the materials to another person or "mirror" the materials on any other server.</li>
                </ul>
                <p>B. This license shall automatically terminate if you violate any of these restrictions and may be
                terminated by us at any time. Upon terminating your viewing of these materials or upon the termination
                of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.
                </p>
                <h4>Disclaimer and Warranties</h4>
                <p>The materials on Site are provided on an 'as is' basis. We makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.</p>
                <p>The data feed for the website is sourced and curated using automated algorithms from various data sources. The comments and opinions are based on automated formulas. These are prone to errors of automation. We are not responsible for the accuracy, reliability, performance, completeness, comprehensiveness, currentness, functionality, continuity, timeliness of the information, data or for any content available.</p>
                <p>Though we make the best efforts for accuracy; the materials appearing on Site could include technical, typographical, or photographic errors. We do not warrant that any of the materials on the Site are accurate, complete or current. We may make changes to the materials contained on Site at any time without notice. However we do not make any commitment to update the materials.</p>
                <p>Further, we do not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.</p>
              </div>
            </Col>
          </Row>
          {/* End Row */}
        </Container>
      </Layout>
    );
  }
}
export default Terms;



export async function getServerSideProps(context) {
  // var Cookies = new Cookies();
  const data = await getMetaData('_metaData', context.req.headers.cookie);
  if (data) {
    var pages = decrypt(data);
    var metaData = pages.filter(page => {
      return page['page_name'] == 'Terms & Condition';
    });
    return {
      props: { metaData: metaData[0] }, // will be passed to the page component as props
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    }
  }

}