import Head from 'next/head'
import React, { Component } from 'react';
import Layout from "../components/Layout/Layout";
import Home from "../components/Home/Home";
import { decrypt } from '_helper/EncrDecrypt';
import { getMetaData } from '_helper/MetaData';

class DefaultPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metaData: {}
    }
  }
  componentDidMount() {
  }
  render() {
    const { metaData } = this.props;
    return (
      <Layout title={metaData && metaData != '' ? metaData['page_title'] : ''} metaKeywords={metaData && metaData != '' ? metaData['page_keyword'] : ''} metaDesc={metaData && metaData != '' ? metaData['page_description'] : ''}>
        <Home />
      </Layout>
    );
  }
}
export default DefaultPage;

export async function getServerSideProps(context) {
  // var Cookies = new Cookies();
  const data = await getMetaData('_metaData', context.req.headers.cookie);
  if (data) {
    var pages = decrypt(data);
    var metaData = pages.filter(page => {
      return page['page_name'] == 'Home';
    });
    return {
      props: { metaData: metaData[0] }, // will be passed to the page component as props
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    }
  }

}