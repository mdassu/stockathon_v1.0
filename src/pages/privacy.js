import Head from 'next/head'
import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Layout from "../components/Layout/Layout";
import Link from 'next/link'
import Cookies from 'js-cookie';
import { decrypt } from '_helper/EncrDecrypt';
import { getMetaData } from '_helper/MetaData';

class Privacy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metaData: {}
    }
  }
  componentDidMount() {
  }
  render() {
    const { metaData } = this.props;
    return (
      <Layout title={metaData && metaData != '' ? metaData['page_title'] : ''} metaKeywords={metaData && metaData != '' ? metaData['page_keyword'] : ''} metaDesc={metaData && metaData != '' ? metaData['page_description'] : ''}>
        {/* Terms Start */}
        <Container>
          {/* Row Start */}
          <Row>
            <Col md="12">
              <div className="terms-content">
                <h1>Privacy Policy</h1>
                <p>Thank you for choosing to be part of our community at Stockathon, doing business as stockathon.in (“stockathon.in”, “we”, “us”, or “our”). We are committed to protecting your personal information and your right to privacy. If you have any questions or concerns about our policy, or our practices with regards to your personal information, please contact us at stockathon.help@gmail.com.</p>
                <p>When you visit our website https://www.stockathon.in, and use our services, you trust us with your personal information. We take your privacy very seriously. In this privacy notice, we describe our privacy policy. We seek to explain to you in the clearest way possible what information we collect, how we use it and what rights you have in relation to it. We hope you take some time to read through it carefully, as it is important. If there are any terms in this privacy policy that you do not agree with, please discontinue use of our Sites and our services.</p>
                <p>This privacy policy applies to all information collected through our website (such as https://www.stockathon.in), and/or any related services, sales, marketing or events (we refer to them collectively in this privacy policy as the "Sites").</p>
                <p>Please read this privacy policy carefully as it will help you make informed decisions about sharing your personal information with us.</p>
                <h4>What information do we collect?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
                <h4>How do we use your information?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
                <h4>Will your information be shared with anyone?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
                <h4>Do we use cookies and other tracking technologies?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
                <h4>How long do we keep your information?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
                <h4>How do we keep your information safe?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
                <h4>Do we collect information from minors?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
                <h4>What are your privacy rights?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
                <h4>Do we make updates to this policy?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
                <h4>How can you contact us about this policy?</h4>
                <p>We may obtain information about you from other sources, such as public databases and from other third parties. Examples of the information we receive from other sources include: social media profile information from social logins; search results and links, including paid listings (such as sponsored links).</p>
              </div>
            </Col>
          </Row>
          {/* End Row */}
        </Container>
      </Layout>
    );
  }
}
export default Privacy;

export async function getServerSideProps(context) {
  // var Cookies = new Cookies();
  const data = await getMetaData('_metaData', context.req.headers.cookie);
  if (data) {
    var pages = decrypt(data);
    var metaData = pages.filter(page => {
      return page['page_name'] == 'Privacy Policy';
    });
    return {
      props: { metaData: metaData[0] }, // will be passed to the page component as props
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    }
  }

}