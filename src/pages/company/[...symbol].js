import React, { Component } from 'react';
import { Button, Table, Container, Row, Col, Form, Modal } from 'react-bootstrap';
import Layout from "components/Layout/Layout";
import { CloudDownload } from '@material-ui/icons';
import Link from 'next/link'
import ProfitAndLoss from 'components/Stock/ProfitAndLoss'
import Router, { withRouter } from 'next/router';
import { StockService } from '_services/StockService';
import { CandleVolume } from 'components/Charts/CandleVolume';
import * as moment from 'moment';
import Loader from 'components/Common/Loader';
import * as Excel from "exceljs/dist/exceljs.min.js";
import * as fs from "file-saver";
import NoStockProfile from 'components/Common/NoStockProfile';
import { AuthenticationService } from '_services/AuthenticationService';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Slide from '@material-ui/core/Slide';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Scrollspy from 'react-scrollspy'
import SocialLogin from 'components/Common/SocialLogin';
import { encrypt } from '_helper/EncrDecrypt';
import Cookies from 'js-cookie';
import AccessTimeIcon from '@material-ui/icons/AccessTime';

// Modal Animation
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

class Stock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      symbol: '',
      stockData: {},
      stockAvailable: 1,
      pnlData: [],
      balanceSheetData: [],
      cashFlowData: [],
      chartData: [],
      pnlMainColumns: [],
      pnlColumns: [],
      pnlRows: [],
      balSheetMainColumns: [],
      balSheetColumns: [],
      balSheetRows: [],
      cashFlowMainColumns: [],
      cashFlowColumns: [],
      cashFlowRows: [],
      newsData: [],
      financialYearData: [],
      pressData: [],
      editColumnList: [],
      defaultColumns: {},
      selectedColumns: {},
      tempSelectedColumns: {},
      dataType: 'daily',
      show: false,
      isLoaded: false,
      noData: false,
      isTableLoaded: false,
      open: false
    }
    this.scrollToElement = this.scrollToElement.bind(this);
  }

  componentDidMount() {
    var symbol = [];
    var tempSymbol = Router.query.symbol[0].replace('-stock-price', '');
    symbol.push(tempSymbol);

    this.setState({
      symbol: symbol.join('/')
    }, () => {
      this.getStockDetails();
      this.getPeersDetails();
      this.getChartData();
      this.getStockNews();
      this.getFinancialYearData();
      this.getPressData();
    })
  }

  componentDidUpdate() {
    var symbol = [];
    var tempSymbol = Router.query.symbol[0].replace('-stock-price', '');
    symbol.push(tempSymbol);
    if (symbol.join('/') != this.state.symbol) {
      this.setState({
        symbol: symbol.join('/')
      }, () => {
        this.setState({
          stockData: {},
          stockAvailable: 1,
          pnlData: [],
          balanceSheetData: [],
          cashFlowData: [],
          chartData: [],
          pnlMainColumns: [],
          pnlColumns: [],
          pnlRows: [],
          balSheetMainColumns: [],
          balSheetColumns: [],
          balSheetRows: [],
          cashFlowMainColumns: [],
          cashFlowColumns: [],
          cashFlowRows: [],
          isLoaded: false,
          noData: false,
          isTableLoaded: false
        });
        this.getStockDetails();
        this.getPeersDetails();
        this.getChartData();
        this.getStockNews();
        this.getFinancialYearData();
      })
    }
  }

  getStockDetails(type = '', tableType = '') {
    Cookies.remove('_stockMetaData');
    var params = {
      symbol: this.state.symbol,
      type: type,
      tableType: tableType
    };
    StockService.getStockDetails(params, res => {
      if (res.status) {
        var data = res.data;
        if (tableType == 'balsheet') {
          this.setState({
            balanceSheetData: data.balanceSheet,
            isTableLoaded: true
          });
        } else {
          var stockMetaData = {
            page_title: 'Title',
            page_name: 'Meta Keywords',
            page_description: 'Meta Description',
          }
          Cookies.set('_stockMetaData', encrypt(stockMetaData));
          this.setState({
            stockData: data.description,
            pnlData: data.pnl,
            balanceSheetData: data.balanceSheet,
            cashFlowData: data.cashflow,
            stockAvailable: res.stockAvailable,
            isLoaded: true,
            isTableLoaded: true
          });
        }
        this.manageExportData(this.state.pnlData, this.state.balanceSheetData, this.state.cashFlowData);
      } else {
        Router.push('/404');
        this.setState({
          noData: true,
          isLoaded: true
        })
      }
    });
  }

  getPeersDetails() {
    var params = {
      symbol: this.state.symbol
    };
    StockService.getPeersDetails(params, res => {
      if (res.status) {
        var data = res.data;
        var defaultColumn = {};
        if (data.savedColumn && Object.keys(data.savedColumn).length > 0) {
          defaultColumn = data.savedColumn
        } else {
          defaultColumn = data.defaultColumn
        }
        this.setState({
          peersData: data.peerData,
          editColumnList: data.filterData,
          selectedColumns: { ...defaultColumn },
          tempSelectedColumns: { ...defaultColumn },
          defaultColumns: { ...data.defaultColumn },
        });
      }
    });
  }

  getChartData(event = null) {
    var dataType = 'daily';
    this.setState({
      chartData: []
    });
    if (event) {
      this.setState({
        dataType: event.target.value
      });
      dataType = event.target.value;
    }
    var params = {
      symbol: this.state.symbol,
      dataType: dataType
    };
    StockService.getChartData(params, res => {
      if (res.status) {
        this.setState({
          chartData: res['data']
        });
      } else {
        this.setState({
          chartData: []
        });
      }
    });
  }

  getFinancialYearData = () => {
    var params = {
      symbol: this.state.symbol
    };
    StockService.getFinancialYear(params, res => {
      if (res.status) {
        this.setState({
          financialYearData: res['data']
        });
      } else {
        this.setState({
          financialYearData: []
        });
      }
    });
  }

  getPressData = () => {
    var params = {
      symbol: this.state.symbol
    };
    StockService.getPressDetails(params, res => {
      if (res.status) {
        this.setState({
          pressData: res['data']
        });
      } else {
        this.setState({
          pressData: []
        });
      }
    });
  }

  manageExportData = (pnlData, balSheetData, cashFlowData) => {
    var pnlMainColumns = [];
    var pnlColumns = [];
    var pnlRows = [];
    var balSheetMainColumns = [];
    var balSheetColumns = [];
    var balSheetRows = [];
    var cashFlowMainColumns = [];
    var cashFlowColumns = [];
    var cashFlowRows = [];
    var firstColumn = ['Narration'];

    // Profit & Loss
    if (pnlData && pnlData.length > 0) {
      var pnlHead = Object.keys(pnlData[0]);
      if (pnlHead.indexOf('innerData') != -1) {
        pnlHead.splice(pnlHead.indexOf('innerData'), 1);
      }
      if (pnlHead.indexOf('type') != -1) {
        pnlHead.splice(pnlHead.indexOf('type'), 1);
      }
      if (pnlHead.indexOf('id') != -1) {
        pnlHead.splice(pnlHead.indexOf('id'), 1);
      }
      if (pnlHead.indexOf('isTotal') != -1) {
        pnlHead.splice(pnlHead.indexOf('isTotal'), 1);
      }
      pnlColumns = firstColumn.concat(pnlHead);

      pnlData.map((data, index) => {
        var dataJson = {};
        pnlColumns.map((item, key) => {
          if (key == 0) {
            dataJson[item] = data['type'];
            if (index == 0) {
              pnlMainColumns.push(this.state.stockData['stock_name']);
            }
          } else {
            dataJson[item] = data[item];
            if (pnlColumns.length - 1 == key && index == 0) {
              pnlMainColumns.push('stockathon.com');
            } else if (index == 0) {
              pnlMainColumns.push('');
            }
          }

        });
        pnlRows.push(dataJson);
      });
    }

    if (balSheetData && balSheetData.length > 0) {

      // Balance Sheet
      var balSheetHead = Object.keys(balSheetData[0]);
      if (balSheetHead.indexOf('innerData') != -1) {
        balSheetHead.splice(balSheetHead.indexOf('innerData'), 1);
      }
      if (balSheetHead.indexOf('type') != -1) {
        balSheetHead.splice(balSheetHead.indexOf('type'), 1);
      }
      if (balSheetHead.indexOf('id') != -1) {
        balSheetHead.splice(balSheetHead.indexOf('id'), 1);
      }
      if (balSheetHead.indexOf('isTotal') != -1) {
        balSheetHead.splice(balSheetHead.indexOf('isTotal'), 1);
      }
      balSheetColumns = firstColumn.concat(balSheetHead);

      balSheetData.map((data, index) => {
        var dataJson = {};
        balSheetColumns.map((item, key) => {
          if (key == 0) {
            dataJson[item] = data['type'];
            if (index == 0) {
              balSheetMainColumns.push(this.state.stockData['stock_name']);
            }
          } else {
            dataJson[item] = data[item];
            if (balSheetColumns.length - 1 == key && index == 0) {
              balSheetMainColumns.push('stockathon.com');
            } else if (index == 0) {
              balSheetMainColumns.push('');
            }
          }

        });
        balSheetRows.push(dataJson);
      });
    }

    if (cashFlowData && cashFlowData.length > 0) {

      // Cash Flow
      var cashFlowHead = Object.keys(cashFlowData[0]);
      if (cashFlowHead.indexOf('innerData') != -1) {
        cashFlowHead.splice(cashFlowHead.indexOf('innerData'), 1);
      }
      if (cashFlowHead.indexOf('type') != -1) {
        cashFlowHead.splice(cashFlowHead.indexOf('type'), 1);
      }
      if (cashFlowHead.indexOf('id') != -1) {
        cashFlowHead.splice(cashFlowHead.indexOf('id'), 1);
      }
      if (cashFlowHead.indexOf('isTotal') != -1) {
        cashFlowHead.splice(cashFlowHead.indexOf('isTotal'), 1);
      }
      cashFlowColumns = firstColumn.concat(cashFlowHead);

      cashFlowData.map((data, index) => {
        var dataJson = {};
        cashFlowColumns.map((item, key) => {
          if (key == 0) {
            dataJson[item] = data['type'];
            if (index == 0) {
              cashFlowMainColumns.push(this.state.stockData['stock_name']);
            }
          } else {
            dataJson[item] = data[item];
            if (cashFlowColumns.length - 1 == key && index == 0) {
              cashFlowMainColumns.push('stockathon.com');
            } else if (index == 0) {
              cashFlowMainColumns.push('');
            }
          }

        });
        cashFlowRows.push(dataJson);
      });
    }

    this.setState({
      pnlMainColumns: pnlMainColumns,
      pnlColumns: pnlColumns,
      pnlRows: pnlRows,
      balSheetMainColumns: balSheetMainColumns,
      balSheetColumns: balSheetColumns,
      balSheetRows: balSheetRows,
      cashFlowMainColumns: cashFlowMainColumns,
      cashFlowColumns: cashFlowColumns,
      cashFlowRows: cashFlowRows,
    });
  }

  scrollToElement(scrollFunc) {
    scrollFunc.scrollIntoView({ behavior: 'smooth', block: "center" });
  }

  exportData = () => {
    var proceed = false;
    if (AuthenticationService.isLogged) {
      var user = AuthenticationService.currentUserValue;
      if (user['membership_flag'] == 1) {
        proceed = true;
      } else {
        Router.push('/premuim');
      }
    } else {
      this.setState({
        show: true
      })
    }
    if (proceed) {
      const fileName = this.state.stockData['stock_name'];
      const EXCEL_EXTENSION = '.xlsx';
      const EXCEL_TYPE =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
      let workbook = new Excel.Workbook();

      // Profit & Loss
      let pnlWorksheet = workbook.addWorksheet("Profit & Loss");

      pnlWorksheet.addRow(this.state.pnlMainColumns);
      pnlWorksheet.addRow([]);

      let pnlHeaderRow = pnlWorksheet.addRow(this.state.pnlColumns);
      // Cell Style : Fill and Border
      pnlHeaderRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: '1C2161' },
        };
        cell.font = {
          color: { argb: 'FFFFFF' },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      });
      this.state.pnlRows.map((element) => {
        let eachRow = [];
        this.state.pnlColumns.map((headers, key) => {
          eachRow.push(element[headers]);
        });
        pnlWorksheet.addRow(eachRow);
      });

      // Balance Sheet
      let balSheetWorksheet = workbook.addWorksheet("Balance Sheet");

      balSheetWorksheet.addRow(this.state.balSheetMainColumns);
      balSheetWorksheet.addRow([]);

      let balSheetHeaderRow = balSheetWorksheet.addRow(this.state.balSheetColumns);
      // Cell Style : Fill and Border
      balSheetHeaderRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: '1C2161' },
        };
        cell.font = {
          color: { argb: 'FFFFFF' },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      });
      this.state.balSheetRows.map((element) => {
        let eachRow = [];
        this.state.balSheetColumns.map((headers, key) => {
          eachRow.push(element[headers]);
        });
        balSheetWorksheet.addRow(eachRow);
      });

      // Cash Flow
      let cashFlowWorksheet = workbook.addWorksheet("Cash Flow");

      cashFlowWorksheet.addRow(this.state.cashFlowMainColumns);
      cashFlowWorksheet.addRow([]);

      let cashFlowHeaderRow = cashFlowWorksheet.addRow(this.state.cashFlowColumns);
      // Cell Style : Fill and Border
      cashFlowHeaderRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: '1C2161' },
        };
        cell.font = {
          color: { argb: 'FFFFFF' },
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      });
      this.state.cashFlowRows.map((element) => {
        let eachRow = [];
        this.state.cashFlowColumns.map((headers, key) => {
          eachRow.push(element[headers]);
        });
        cashFlowWorksheet.addRow(eachRow);
      });

      // worksheet.addRow([]);
      //Generate Excel File with given name
      workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: EXCEL_TYPE });
        fs.saveAs(
          blob,
          fileName + EXCEL_EXTENSION
        );
      });
    }
  }

  changeTableView = (e, tableType) => {

    if (tableType == 'balsheet') {
      var proceed = false;
      if (e.target.value == 'quarterly') {
        if (AuthenticationService.isLogged) {
          var user = AuthenticationService.currentUserValue;
          if (user['membership_flag'] == 1) {
            proceed = true;
          } else {
            Router.push('/premuim');
          }
        } else {
          this.setState({
            show: true
          })
        }
      } else {
        proceed = true;
      }

      if (proceed) {
        this.setState({
          balanceSheetData: [],
          balSheetMainColumns: [],
          balSheetColumns: [],
          balSheetRows: [],
          isTableLoaded: false
        }, () => {
          this.getStockDetails(e.target.value, tableType);
        });
      }

    }
  }

  getStockNews = () => {
    var params = {
      symbol: this.state.symbol
    }
    StockService.getStockNews(params, res => {
      if (res.status) {
        this.setState({
          newsData: res['data']
        })
      } else {
        this.setState({
          newsData: []
        })
      }
    });
  }

  handleClose = () => {
    this.setState({
      show: false
    });
  }

  checkEditColumn = name => ({ target: { checked, value } }) => {
    var column = this.state.tempSelectedColumns;
    if (column[name] && !checked) {
      delete column[name]
      this.setState({
        tempSelectedColumns: { ...column }
      });
    } else {
      column[name] = value;
      this.setState({
        tempSelectedColumns: { ...column }
      });
    }
  }

  saveColumn = () => {
    var params = {
      peerColumns: JSON.stringify(this.state.tempSelectedColumns)
    };
    StockService.savePeerColumns(params, res => {
      if (res.status) {

      }
    });
    this.setState({
      selectedColumns: { ...this.state.tempSelectedColumns }
    });
    this.handleCloseModal();
  }

  removeColumn = (column) => {
    var col = this.state.tempSelectedColumns;
    delete col[column];
    this.setState({
      tempSelectedColumns: { ...col }
    });
  }

  resetColum = () => {
    this.setState({
      tempSelectedColumns: { ...this.state.defaultColumns }
    });
  }

  handleClickOpen = () => {
    if (AuthenticationService.isLogged) {
      var user = AuthenticationService.currentUserValue;
      if (user['membership_flag'] == 1) {
        this.setState({
          open: true,
        });
      } else {
        Router.push('/premuim');
      }
    } else {
      this.setState({
        show: true
      });
    }
  };

  handleCloseModal = e => {
    this.setState({
      open: false
    });
  };

  handleCancelModal = e => {
    this.setState({
      open: false,
      tempSelectedColumns: { ...this.state.selectedColumns }
    });
  };

  render() {
    const { stockData, stockAvailable, chartData, peersData, pnlData, balanceSheetData, cashFlowData, newsData, isLoaded, isTableLoaded, noData, show, open, financialYearData, pressData, editColumnList, selectedColumns, tempSelectedColumns, dataType } = this.state;
    const { metaData } = this.props;
    var allItemRows = '';
    if (peersData && peersData.length > 0) {
      allItemRows = peersData.map((data, key) =>
        <tr key={key}>
          <td className="text-left">{key + 1}</td>
          <td className='peer-active-td text-left'><Link href={`/company/${data['symbol']}`}>{data.stock_name}</Link></td>
          <td>{data.cmp}</td>
          {
            Object.keys(selectedColumns).map((item, index) =>
              <td key={'a' + index}>{data[item]}</td>
            )
          }
          {/* <td>{data.peNormalizedAnnual}</td>
          <td>{data.pbAnnual}</td>
          <td>{data.marketCapitalization}</td>
          <td>{data.currentDividendYieldTTM}</td>
          <td>{data.sales}</td>
          <td>{data.roi}</td> */}
        </tr >
      );
    }

    var allNewsData = '';
    if (newsData && newsData.length > 0) {
      allNewsData = newsData.map((news, key) =>
        <li key={key}>
          <span className="event-icon">
            {/* <svg xmlns="http://www.w3.org/2000/svg" width="" height="" fill="currentColor" class="bi bi-calendar2-check" viewBox="0 0 16 16">
              <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
              <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
              <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
            </svg> */}
            <img src={news['image']} width="" alt="Img" />
          </span>
          <a href={news['url']} target="_blank">{news['headline']} <span>Announced On {moment(news['datetime']).format('MMM DD, YYYY')}</span>
            <p>{news['summary']}</p>
          </a>
        </li>
      )
    }

    return (
      <Layout title={metaData ? metaData['page_title'] : ''} metaKeywords={metaData ? metaData['page_keyword'] : ''} metaDesc={metaData ? metaData['page_description'] : ''}>
        {
          isLoaded ?
            (
              stockAvailable == 1 ?
                (
                  Object.keys(stockData) && Object.keys(stockData).length > 0 ? (
                    <section className="profile-main-section">
                      <section className="profile-banner-section">
                        <Container>
                          {/* Row Start */}
                          <Row>
                            <Col lg="12" className="py-4">
                              <Row>
                                <Col lg="8" md="8">
                                  <div className="profile-name">
                                    {
                                      stockData.logo != '' ?
                                        (
                                          <div className="stock-image">
                                            <img src={stockData.logo} alt='Stock-img' title="" />
                                          </div>
                                        ) : ''
                                    }
                                    <div className="banner-heading">

                                      <h1>{stockData.stock_name} <span className="mr-2">{stockData.symbol}</span>

                                        {
                                          stockData.weburl != '' ?
                                            (
                                              <a href={stockData.weburl} title="Web URL" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-box-arrow-up-right" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z" />
                                                <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z" />
                                              </svg></a>
                                            ) : ''
                                        }
                                      </h1>
                                      <h2>{stockData.close} <span className={stockData.change < 0 ? 'low' : 'high'}>
                                        {
                                          stockData.change < 0 ? (
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-down-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
                                            </svg>
                                          ) : (
                                              <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-up-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.247 4.86l-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z" />
                                              </svg>
                                            )
                                        }
                                        {stockData.change} ({stockData.changeper}%)</span></h2>
                                      {/* <h2>14.35 <span className="low"> 0.20 (1.41%)</span></h2> */}
                                    </div>
                                  </div>

                                </Col>
                                <Col lg="4" md="4">
                                  <div className="banner-btns">
                                    {/* <Button className="save-alert" title="Save Alert" onClick={this.exportData}><NotificationsIcon /> Save Alert</Button>
                              <Button className="watchlist-btn" title="Add To Watchlist"><BookmarkIcon /> Add To Watchlist</Button> */}
                                    <Button className="watchlist-btn" title="Export to Excel" onClick={this.exportData}><CloudDownload /> Export to Excel</Button>
                                  </div>
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        </Container>
                      </section>
                      <section className="sticky">
                        <Container>
                          <Row>
                            <Col lg="12">

                              <div className="profile-tabs table-responsive">
                                <Scrollspy items={['overview', 'chart', 'peers', 'profit-loss', 'balance-sheet', 'cash-flow']} offset={-100} currentClassName="active-tab">
                                  {/* <ul> */}
                                  <li className="active-tab">
                                    <a title="Overview" href="#overview">Overview</a>
                                  </li>
                                  <li>
                                    <a title="Chart" href="#chart">Chart</a>
                                  </li>
                                  <li>
                                    <a title="Peers" href="#peers">Peers</a>
                                  </li>
                                  {/* <li>
                                      <a title="Analysis" href="#analysis">Analysis</a>
                                    </li>
                                    <li>
                                      <a title="Peers" href="#peers">Peers</a>
                                    </li>
                                    <li>
                                      <a title="Quarters" href="#quarters">Quarters</a>
                                   </li> */}
                                  <li>
                                    <a title="Profile & Loss" href="#profit-loss">Profile & Loss</a>
                                  </li>
                                  <li>
                                    <a title="Balance Sheet" href="#balance-sheet">Balance Sheet</a>
                                  </li>
                                  <li>
                                    <a title="Cash Flow" href="#cash-flow">Cash Flow</a>
                                  </li>
                                  {/* <li>
                                    <a title="Ratios" href="#ratios">Ratios</a>
                                  </li> */}
                                  {/* </ul> */}
                                </Scrollspy>
                              </div>

                            </Col>
                          </Row>
                        </Container>
                      </section>

                      <section className="profile-content-section">
                        {/* Overview */}
                        <Container id="overview">
                          <Row>
                            <Col md="12">
                              <div className="profile-detail-box">
                                <Row>
                                  <Col lg="4" md="6">
                                    <div className="overview-box">
                                      <h5 className="table-heading">Overview</h5>
                                      <ul className="overview-ul">
                                        <li>
                                          <Link href="#"><a>Market cap <span>${stockData['marketCapitalization']} mn</span></a></Link>
                                        </li>
                                        <li>
                                          <Link href="#"><a className="active-row">52 Week Range <span>{`${stockData['_52WeekLow']} ~ ${stockData['_52WeekHigh']}`}</span></a></Link>
                                        </li>
                                        <li>
                                          <Link href="#"><a>Beta <span>{stockData['beta']}</span></a></Link>
                                        </li>
                                        <li>
                                          <Link href="#"><a className="active-row">P/E <span>{stockData['peNormalizedAnnual']}</span></a></Link>
                                        </li>
                                        <li>
                                          <Link href="#"><a>P/B <span>{stockData['pbAnnual']}</span></a></Link>
                                        </li>
                                        <li>
                                          <Link href="#"><a className="active-row">ROI <span>{stockData['roiAnnual']}</span></a></Link>
                                        </li>
                                        <li>
                                          <Link href="#"><a>Dividend Yield <span>{stockData['currentDividendYieldTTM']}</span></a></Link>
                                        </li>
                                        <li>
                                          <Link href="#"><a className="active-row">Current Ratio <span>{stockData['currentRatioAnnual']}</span></a></Link>
                                        </li>
                                        <li>
                                          <Link href="#"><a>Quick Ratio <span>{stockData['quickRatioAnnual']}</span></a></Link>
                                        </li>
                                        <li>
                                          <Link href="#"><a className="active-row">Shares Outstanding <span>{stockData['shareOutstanding']} mn</span></a></Link>
                                        </li>
                                      </ul>
                                    </div>
                                  </Col>
                                  <Col lg="8" md="6">
                                    <div className="overview-box">
                                      <h5 className="table-heading">Profile</h5>
                                      <div className="profile-ratio">
                                        <ul>
                                          <li>
                                            <h6>Sector</h6>
                                            <span>{stockData['gsector']}</span>
                                          </li>
                                          <li>
                                            <h6>Industry</h6>
                                            <span>{stockData['finnhubIndustry']}</span>
                                          </li>
                                          <li>
                                            <h6>Sub Industry</h6>
                                            <span>{stockData['gsubind']}</span>
                                          </li>
                                          <li>
                                            <h6># of Employees</h6>
                                            <span>{stockData['employeeTotal']}</span>
                                          </li>
                                          <li>
                                            <h6>IPO Date</h6>
                                            <span>{moment(stockData['ipo']).format('YYYY-MM-DD')}</span>
                                          </li>
                                        </ul>
                                      </div>
                                      <p>{stockData['company_description']}</p>
                                    </div>
                                  </Col>
                                </Row>
                              </div>
                            </Col>
                          </Row>
                        </Container>
                        {/* Chart Data */}
                        <Container id="chart">
                          <Row>
                            {
                              chartData.length > 0 ? (
                                <div className="col-lg-12">
                                  <div className="table-box  volume-chart-box">
                                    <div className="table-view">
                                      <Form.Control as="select" value={dataType} className="range-select" onChange={($event) => this.getChartData($event)}>
                                        <option value="daily" selected={dataType == 'daily' ? true : false}>Daily</option>
                                        <option value="weekly" selected={dataType == 'weekly' ? true : false}>Weekly</option>
                                        <option value="monthly" selected={dataType == 'monthly' ? true : false}>Monthly</option>
                                      </Form.Control>
                                    </div>
                                    <div className="clearfix"></div>
                                    {
                                      chartData.length > 0 ? (
                                        <CandleVolume chartData={chartData} />
                                      ) : ''
                                    }
                                  </div>
                                </div>
                              ) : (
                                  <div className="col-lg-12">
                                    <Loader />
                                  </div>
                                )
                            }

                          </Row>
                        </Container>
                        {
                          !noData ? (
                            <React.Fragment>

                              {/* Peers Comparison */}
                              <Container id="peers">
                                <Row>
                                  <Col md="12">
                                    <div className="table-box">
                                      <Row>
                                        <Col xs="6">
                                          <h5 className="table-heading">Peer Comparison</h5>
                                          {/* <p className="table-p">Consolidated Figures in Rs. Crores / <Link href="#"><a>View Standalone</a></Link> </p> */}
                                        </Col>
                                        <Col xs="6" className="text-right">
                                          <Button className="table-button mb-2" title="Edit Columns" onClick={this.handleClickOpen}>Edit Columns</Button>
                                        </Col>
                                      </Row>
                                      <div className="table-responsive">
                                        <Table>
                                          <thead>
                                            <tr>
                                              <th className="text-left">S.No</th>
                                              <th className="text-left">Name</th>
                                              <th className="text-right">CMP</th>
                                              {
                                                selectedColumns && Object.keys(selectedColumns).length > 0 ? (
                                                  Object.keys(selectedColumns).map((item, key) => {
                                                    return (
                                                      <th className="text-right" key={key}>{selectedColumns[item]}</th>
                                                    )
                                                  })
                                                ) : ''
                                              }
                                            </tr>
                                          </thead>
                                          <tbody>
                                            {allItemRows}
                                          </tbody>
                                        </Table>
                                      </div>
                                    </div>
                                  </Col>
                                </Row>
                              </Container>

                              <Container id="profit-loss">
                                <Row>
                                  <Col lg="12">
                                    <div className="table-box">
                                      <Row>
                                        <Col md="6">
                                          <h5 className="table-heading">Profit & Loss</h5>
                                          <p className="table-p">Consolidated Figures in Million Dollar
                                     {/* <Link href="#"><a>View Standalone</a></Link>  */}
                                          </p>
                                        </Col>
                                        {/* <Col md="6">
                                  <Button className="table-button" title="Product Segments">Product Segments</Button>
                                </Col> */}
                                      </Row>
                                      {pnlData && pnlData.length > 0 ? (<ProfitAndLoss data={pnlData} />) : ''}
                                    </div>
                                  </Col>
                                </Row>
                              </Container>
                              {/* BalanceSheet */}

                              <Container id="balance-sheet">
                                <Row>
                                  <Col md="12">
                                    <div className="table-box">
                                      <Row>
                                        <Col md="6">
                                          <h5 className="table-heading">Balance Sheet</h5>
                                          <p className="table-p">Consolidated Figures in Million Dollar
                                    {/* <Link href="#"><a>View Standalone</a></Link> */}
                                          </p>
                                        </Col>
                                        <Col md="6">
                                          {/* <Button className="table-button" title="Corporate Actions">Corporate Actions</Button> */}
                                          <div className="table-view">
                                            <Form.Label>Table View</Form.Label>
                                            <Form.Control as="select" onChange={($event) => this.changeTableView($event, 'balsheet')}>
                                              <option value="">Annually</option>
                                              <option value="quarterly">Quarterly</option>
                                            </Form.Control>
                                          </div>
                                        </Col>
                                      </Row>
                                      {isTableLoaded ? balanceSheetData && balanceSheetData.length > 0 ? (<ProfitAndLoss data={balanceSheetData} />) : '' : <Loader />}
                                    </div>
                                  </Col>
                                </Row>
                              </Container>
                              {/* <BalanceSheet /> */}

                              {/* Cash Flows */}
                              <Container id="cash-flow">
                                <Row>
                                  <Col md="12">
                                    <div className="table-box">
                                      <Row>
                                        <Col md="6">
                                          <h5 className="table-heading">Cash Flows</h5>
                                          <p className="table-p">Consolidated Figures in Million Dollar
                                    {/* <Link href="#"><a>View Standalone</a></Link>  */}
                                          </p>
                                        </Col>
                                      </Row>
                                      {cashFlowData && cashFlowData.length > 0 ? (<ProfitAndLoss data={cashFlowData} />) : ''}
                                    </div>
                                  </Col>
                                </Row>
                              </Container>

                              {/* News And events */}
                              {
                                allNewsData ? (
                                  <Container>
                                    <Row>
                                      <Col md="12">
                                        <div className="profile-detail-box">
                                          <Row>
                                            {/* <Col lg="6">
                                          <div className="overview-box news-box">
                                            <h5 className="table-heading">Recent News</h5>
                                            <ul className="news-ul">
                                              <li>
                                                <img src="/images/yes-bank.jpg" width="" alt="Img" />
                                                <a href="#">Market Movers: YES Bank shares extend rally; 381 stocks hit upper circuit <span>1 hour ago • Economic Times</span></a>
                                              </li>
                                              <li>
                                                <img src="/images/yes-bank.jpg" width="" alt="Img" />
                                                <a href="#">Market Movers: YES Bank shares extend rally; 381 stocks hit upper circuit <span>1 hour ago • Economic Times</span></a>
                                              </li>
                                              <li>
                                                <img src="/images/yes-bank.jpg" width="" alt="Img" />
                                                <a href="#">Market Movers: YES Bank shares extend rally; 381 stocks hit upper circuit <span>1 hour ago • Economic Times</span></a>
                                              </li>
                                            </ul>
                                            <Link href="#"><a title="View All" className="news-view-all">View All</a></Link>
                                          </div>
                                        </Col> */}
                                            <Col lg="12">
                                              <div className="overview-box news-box">
                                                <h5 className="table-heading">Latest on {stockData['stock_name']}</h5>
                                                <ul className="news-ul events-ul">
                                                  {
                                                    allNewsData ? allNewsData : ''
                                                  }
                                                </ul>
                                                {/* <Link href="#"><a title="View All" className="news-view-all">View All</a></Link> */}
                                              </div>
                                            </Col>

                                          </Row>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Container>
                                ) : ''
                              }
                              {
                                ((financialYearData && financialYearData.length > 0) || (pressData && pressData.length > 0)) ? (
                                  <Container>
                                    <Row>
                                      <Col md="12">
                                        <div className="profile-detail-box">
                                          <Row>
                                            {
                                              financialYearData && financialYearData.length > 0 ? (
                                                <Col lg="4">
                                                  <div className="overview-box news-box">
                                                    <h5 className="table-heading">Annual reports</h5>
                                                    <ul className="news-ul events-ul">
                                                      {
                                                        financialYearData.map((item, key) => {
                                                          return (
                                                            <li key={key}>
                                                              <a href={item.reportUrl} target="_blank">Financial Year {moment(item.filedDate).format('YYYY')}
                                                                {/* <span>from bse</span> */}
                                                              </a>
                                                            </li>
                                                          );
                                                        })
                                                      }
                                                    </ul>
                                                  </div>
                                                </Col>
                                              ) : ''
                                            }
                                            {
                                              pressData && pressData.length > 0 ? (
                                                <Col lg="8">
                                                  <div className="overview-box news-box">
                                                    <h5 className="table-heading">Press Release</h5>
                                                    <ul className="news-ul events-ul">
                                                      {
                                                        pressData.map((item, key) => {
                                                          return (
                                                            <li key={key}>
                                                              <a>{item.headline}
                                                                <span>{item.description}</span>
                                                                <span><AccessTimeIcon />{moment(item.datetime).format('MMM DD, YYYY HH:MM:AA')}</span>
                                                              </a>
                                                            </li>
                                                          );
                                                        })
                                                      }
                                                    </ul>
                                                  </div>
                                                </Col>
                                              ) : ''
                                            }
                                          </Row>
                                        </div>
                                      </Col>
                                    </Row>
                                  </Container>
                                ) : ''
                              }
                            </React.Fragment>
                          ) : ''
                        }
                        <Modal show={show} onHide={this.handleClose} className="popup-login" >
                          <Modal.Header closeButton className="border-0 p-0">
                            {/* <Modal.Title>Stockathon</Modal.Title> */}
                          </Modal.Header>
                          <Modal.Body>
                            {/* <div className="text-center">
                              You need to login to access this feature.
                              <div className="header-btns mt-2">
                                <Link href="/login">
                                  <Button className="header-register-btn m-auto" title="LogIn / Register">Login / Register</Button>
                                </Link>
                              </div>
                            </div> */}
                            <SocialLogin hidePopup={() => this.setState({ show: false })} />
                          </Modal.Body>
                        </Modal>
                        {/* Filter Modal */}
                        <Dialog
                          open={open}
                          onClose={this.handleCloseModal}
                          aria-describedby="filter-dialog"
                          TransitionComponent={Transition}
                        >
                          <DialogContent className="filter-dialog">
                            <DialogContentText id="filter-dialog">
                              <section className="filters-section">
                                <Container>
                                  <Row>
                                    <Col>
                                      <Form>
                                        <Row>
                                          <Col md="6">
                                            <h1>Manage Columns</h1>
                                          </Col>
                                          <Col md="6">
                                            <div className="column-btns">
                                              <Button className="save-button" onClick={this.saveColumn}>Save Columns</Button>
                                              <Button className="cancel-button" onClick={this.handleCancelModal}>Cancel</Button>
                                              <Button className="cancel-button" onClick={this.resetColum}>Reset Defaults</Button>
                                            </div>
                                          </Col>
                                        </Row>
                                        <div className="manage-columns">
                                          <div className="saved-columns">
                                            <h4>Applied Filters :</h4>
                                            <ul>
                                              {
                                                tempSelectedColumns && Object.keys(tempSelectedColumns) ?
                                                  Object.keys(tempSelectedColumns).map((item, key) => {
                                                    return (<li key={key}>{tempSelectedColumns[item]} <span className="close_icon" onClick={() => this.removeColumn(item)}></span></li>)
                                                  }) : ''
                                              }
                                              {/* <li>Book Value Per Share <span className="close_icon"></span></li>
                                              <li>Gross Margin <span className="close_icon"></span></li>
                                              <li>Operating Margin <span className="close_icon"></span></li>
                                              <li>Market Cap <span className="close_icon"></span></li>
                                              <li>52 week High <span className="close_icon"></span></li>
                                              <li>10 day Avg Trading Volume <span className="close_icon"></span></li>
                                              <li>Sales Growth 5 Yrs <span className="close_icon"></span></li>
                                              <li>EPS Growth 3 Yrs <span className="close_icon"></span></li>
                                              <li>Current Ratio <span className="close_icon"></span></li> */}
                                            </ul>
                                          </div>
                                          <div className="card-columns">
                                            <div className="card">
                                              <h4>Financials</h4>
                                              {
                                                editColumnList && editColumnList.financial ?
                                                  Object.keys(editColumnList.financial).map((item, key) => {
                                                    return (
                                                      <FormControlLabel
                                                        key={key}
                                                        control={
                                                          <Checkbox
                                                            name={item}
                                                            color="primary"
                                                            value={editColumnList['financial'][item]}
                                                            onChange={this.checkEditColumn(item)}
                                                            checked={tempSelectedColumns[item] ? true : false}
                                                          />
                                                        }
                                                        label={editColumnList['financial'][item]}
                                                      />
                                                    )
                                                  })
                                                  : ''
                                              }
                                            </div>
                                            <div className="card">
                                              <h4>Price Action</h4>
                                              {
                                                editColumnList && editColumnList.priceAction ?
                                                  Object.keys(editColumnList.priceAction).map((item, key) => {
                                                    return (
                                                      <FormControlLabel
                                                        key={key}
                                                        control={
                                                          <Checkbox
                                                            name={item}
                                                            color="primary"
                                                            value={editColumnList['priceAction'][item]}
                                                            onChange={this.checkEditColumn(item)}
                                                            checked={tempSelectedColumns[item] ? true : false}
                                                          />
                                                        }
                                                        label={editColumnList['priceAction'][item]}
                                                      />
                                                    )
                                                  })
                                                  : ''
                                              }
                                            </div>
                                            <div className="card">
                                              <h4>Historical</h4>
                                              {
                                                editColumnList && editColumnList.historical ?
                                                  Object.keys(editColumnList.historical).map((item, key) => {
                                                    return (
                                                      <FormControlLabel
                                                        key={key}
                                                        control={
                                                          <Checkbox
                                                            name={item}
                                                            color="primary"
                                                            value={editColumnList['historical'][item]}
                                                            onChange={this.checkEditColumn(item)}
                                                            checked={tempSelectedColumns[item] ? true : false}
                                                          />
                                                        }
                                                        label={editColumnList['historical'][item]}
                                                      />
                                                    )
                                                  })
                                                  : ''
                                              }
                                            </div>
                                            <div className="card">
                                              <h4>Ratios</h4>
                                              {
                                                editColumnList && editColumnList.ratios ?
                                                  Object.keys(editColumnList.ratios).map((item, key) => {
                                                    return (
                                                      <FormControlLabel
                                                        key={key}
                                                        control={
                                                          <Checkbox
                                                            name={item}
                                                            color="primary"
                                                            value={editColumnList['ratios'][item]}
                                                            onChange={this.checkEditColumn(item)}
                                                            checked={tempSelectedColumns[item] ? true : false}
                                                          />
                                                        }
                                                        label={editColumnList['ratios'][item]}
                                                      />
                                                    )
                                                  })
                                                  : ''
                                              }
                                            </div>
                                          </div>

                                        </div>
                                      </Form>
                                    </Col>
                                  </Row>
                                </Container>
                              </section>
                            </DialogContentText>
                          </DialogContent>
                        </Dialog>
                      </section>
                    </section>

                  ) : ''
                ) : (
                  <NoStockProfile stockData={stockData} />
                )
            ) : (
              <Loader />
            )
        }
      </Layout >

    );
  }
}
export default withRouter(Stock);

export async function getServerSideProps(context) {

  var tempSymbol = context.query['symbol'][0];
  var symbol = tempSymbol.split('-')[0];
  var url = `${process.env.POST_API_URL}getStockMetaData`;
  var metaData = {};
  var response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ symbol: symbol }) // body data type must match "Content-Type" header
  });

  var data = await response.json();
  metaData = data['data'];

  if (metaData) {
    return {
      props: {
        metaData: metaData
      }
    }
  } else {
    return {
      props: {

      }, // will be passed to the page component as props
    };
  }
  // var Cookies = new Cookies();
  // return response;

}