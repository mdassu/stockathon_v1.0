import Head from 'next/head'
import React, { Component } from 'react';
import { Col, Container, Form, Row, Button } from 'react-bootstrap';
import Layout from "../components/Layout/Layout";

class NoAnalysis extends React.Component {
  render() {
    return (
      <Layout>
        <section className="profile-banner-section no-analysis-section">
          <Container>
            {/* Row Start */}
            <Row>
              <Col lg="12" className="py-4">
                <Row>
                  <Col lg="12">
                    <div className="banner-heading">
                      <h1 className="py-2">Yes Bank Ltd <span>YESBANK</span></h1>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Container>
        </section>
        <section>
          <Container>
            <Row>
              <Col md="12">
                <div className="no-analysis">
                  <h2>No anlysis available !</h2>
                  <p>There is no analysis available for this stock. Please contact the administrator for more details.</p>
                  <Form>
                    <Form.Row>
                      <Col md="4">
                        <Form.Control placeholder="First name" />
                      </Col>
                      <Col md="4">
                        <Form.Control placeholder="Last name" />
                      </Col>
                      <Col md="4">
                        <Form.Control type="email" placeholder="Email" />
                      </Col>
                      <Col md="12">
                        <Form.Control as="textarea" rows={5} placeholder="Message" />
                      </Col>
                      <Col md="12">
                        <Button>Submit</Button>
                      </Col>
                    </Form.Row>
                  </Form>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </Layout>
    );
  }
}
export default NoAnalysis;