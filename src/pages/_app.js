import App from 'next/app';
import React from 'react';
import Router from 'next/router';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/globals.scss';
import { AuthenticationService } from '_services/AuthenticationService';
import Head from 'next/head';
import { AuthProvider, ProtectRoute } from '_helper/Auth';


export default class MyApp extends App {
  constructor(props) {
    super(props);
    this.state = {
      // isLoggedIn: false,
      continue: true,
      prevRoute: '',
    }
  }

  // componentDidMount() {
  //   this.checkRoute();
  // }

  // componentDidUpdate(prevProps) {
  //   if (this.state.prevRoute != Router.pathname) {
  //     this.checkRoute();
  //   }
  // }

  // checkRoute = () => {
  //   var prevRoute = Router.pathname;
  //   if (Router.pathname != '/' && Router.pathname != '/login') {
  //     if (AuthenticationService.isLogged) {
  //       AuthenticationService.currentUser.subscribe(user => {
  //         if (user && user != null) {
  //           this.setState({
  //             // isLoggedIn: true,
  //             continue: true,
  //             prevRoute: prevRoute
  //           });
  //           if (Router.pathname == '/login') {
  //             Router.push('/').then(() => {
  //               this.setState({
  //                 continue: true,
  //                 prevRoute: prevRoute
  //               });
  //             });
  //           }
  //         } else {
  //           Router.push('/').then(() => {
  //             this.setState({
  //               continue: true,
  //               prevRoute: prevRoute
  //             });
  //           });
  //         }
  //       });
  //     } else {
  //       this.setState({
  //         continue: true,
  //         prevRoute: prevRoute
  //       });
  //     }

  //   } else {
  //     if (AuthenticationService.isLogged && (Router.pathname == '/login')) {
  //       Router.push('/').then(() => {
  //         this.setState({
  //           continue: true,
  //           prevRoute: prevRoute
  //         });
  //       });
  //     } else {
  //       this.setState({
  //         continue: true,
  //         prevRoute: prevRoute
  //       });
  //     }

  //   }
  // }

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render() {
    const { Component, pageProps } = this.props

    return (
      <React.Fragment>
        <Head>
          {/* <!-- Global site tag (gtag.js) - Google Analytics --> */}
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-48143245-1"></script>
          <script dangerouslySetInnerHTML={
            {
              __html: `
                      window.dataLayer = window.dataLayer || [];
                      function gtag(){dataLayer.push(arguments);}
                      gtag('js', new Date());
                    
                      gtag('config', 'UA-48143245-1');
                    `
            }
          } />
        </Head>
        {/* {this.state.continue ? (<Component {...pageProps} />) : ''} */}
        <AuthProvider>
          <ProtectRoute>
            <Component {...pageProps} />
          </ProtectRoute>
        </AuthProvider>
      </React.Fragment>
    )
  }
}

