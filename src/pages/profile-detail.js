import Head from 'next/head'
import React, { Component } from 'react';
import { Button, Table, Container, Row, Col } from 'react-bootstrap';
import Layout from "../components/Layout/Layout";
import BookmarkIcon from '@material-ui/icons/Bookmark';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Link from 'next/link'
import BalanceSheet from '../components/Stock/BalanceSheet'
import ProfitAndLoss from '../components/Stock/ProfitAndLoss'
import CandleChart from 'components/Charts/CandleChart';
import { CandleVolume } from 'components/Charts/CandleVolume';


class ProfileDetail extends React.Component {

  constructor(props) {
    super(props);

    // For Onclick scroll Element
    this.scrollToElement = this.scrollToElement.bind(this);
  }
  // Onclick scroll Element
  scrollToElement(scrollFunc) {
    scrollFunc.scrollIntoView({ behavior: 'smooth' });
  }
  // End Onclick scroll Element

  render() {

    return (

      <Layout>
        <section className="profile-main-section">
          <section className="profile-banner-section">
            <Container>
              {/* Row Start */}
              <Row>
                <Col lg="12" className="py-4">
                  <Row>
                    <Col lg="6" md="6">
                      <div className="banner-heading">
                        <h1>Yes Bank Ltd <span>YESBANK</span> <a href="#" title="Web URL"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-box-arrow-up-right" viewBox="0 0 16 16">
                          <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z" />
                          <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z" />
                        </svg></a></h1>
                        <h2>14.35 <span className="high"><svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-up-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path d="M7.247 4.86l-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z" />
                        </svg> 0.20 (1.41%)</span><b> ISIN : US0378331005</b></h2>
                        {/* <h2>14.35 <span className="low"><svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-down-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
                        </svg> 0.20 (1.41%)</span></h2> */}
                      </div>
                    </Col>
                    <Col lg="6" md="6">
                      <div className="banner-btns">
                        <Button className="save-alert" title="Save Alert"><NotificationsIcon /> Save Alert</Button>
                        <Button className="watchlist-btn" title="Add To Watchlist"><BookmarkIcon /> Add To Watchlist</Button>
                      </div>
                    </Col>
                  </Row>
                </Col>
                <Col lg="12">
                  <div className="profile-ratio">
                    <ul>
                      <li>
                        <h5>Sector</h5>
                        <span>Information Technology</span>
                      </li>
                      <li>
                        <h5>Industry</h5>
                        <span>Technology</span>
                      </li>
                      <li>
                        <h5>Sub Industry</h5>
                        <span>Technology Hardware, Storage & Peripherals</span>
                      </li>
                      <li>
                        <h5># of Employees</h5>
                        <span>137,000</span>
                      </li>
                      <li>
                        <h5>IPO Date</h5>
                        <span>1980-12-12</span>
                      </li>
                      {/* <li>
                        <h5>Sector Div Yld</h5>
                        <span>0.71%</span>
                      </li> */}
                    </ul>
                  </div>
                </Col>
              </Row>
            </Container>
          </section>
          <section className="sticky">
            <Container>
              <Row>
                <Col lg="12">
                  <div className="profile-tabs table-responsive">
                    <ul>
                      <li>
                        <a title="Yes Bank" className="active-tab" onClick={() => this.scrollToElement(this.yesBank)}>Yes Bank</a>
                      </li>
                      <li>
                        <a title="Chart" onClick={() => this.scrollToElement(this.chart)}>Chart</a>
                      </li>
                      <li>
                        <a title="Analysis">Analysis</a>
                      </li>
                      <li>
                        <a title="Peers">Peers</a>
                      </li>
                      <li>
                        <a title="Quarters">Quarters</a>
                      </li>
                      <li>
                        <a title="Profile & Loss">Profile & Loss</a>
                      </li>
                      <li>
                        <a title="Balance Sheet">Balance Sheet</a>
                      </li>
                      <li>
                        <a title="Cash Flow">Cash Flow</a>
                      </li>
                      <li>
                        <a title="Ratios">Ratios</a>
                      </li>
                    </ul>
                  </div>
                </Col>
              </Row>
            </Container>
          </section>
          <section className="profile-content-section">
            {/* Overview */}
            <Container ref={(scrollFunc) => this.yesBank = scrollFunc}>
              <Row>
                <Col md="12">
                  <div className="profile-detail-box">
                    <Row>
                      <Col lg="7">
                        <div className="overview-box">
                          <h5 className="table-heading">Overview</h5>
                          <ul className="overview-ul">
                            <li>
                              <Link href="#"><a>Market cap <span>US$ 2,103,973</span></a></Link>
                            </li>
                            <li>
                              <Link href="#"><a>52 Week Range <span>53.1525 ~ 137.98</span></a></Link>
                            </li>
                            <li className="active-row">
                              <Link href="#"><a>Beta  <span>1.27192</span></a></Link>
                            </li>
                            <li className="active-row">
                              <Link href="#"><a>P/E <span>37.78222</span></a></Link>
                            </li>
                            <li>
                              <Link href="#"><a>P/B <span>32.15345</span></a></Link>
                            </li>
                            <li>
                              <Link href="#"><a>ROI <span>25.44284</span></a></Link>
                            </li>
                            <li className="active-row">
                              <Link href="#"><a>Dividend Yield <span>0.64242</span></a></Link>
                            </li>
                            <li className="active-row">
                              <Link href="#"><a>Current Ratio <span>1.3636</span></a></Link>
                            </li>
                            <li className="">
                              <Link href="#"><a>Quick Ratio <span>1.32507</span></a></Link>
                            </li>
                            <li className="">
                              <Link href="#"><a>Share Outstanding <span>17,001,802,000</span></a></Link>
                            </li>
                          </ul>
                        </div>
                      </Col>
                      <Col lg="5">
                        <div className="overview-box">
                          <h5 className="table-heading">Profile</h5>
                          <p>The Bank was incorporated in the year 2004 by
                          Rana Kapoor and Late Ashok Kapur, which is a new
                          age private sector bank. And since the inception of the
                          bank has fructified into a “Full-Service Commercial Bank,”
                          it has steadily built Corporate and Institutional
                          Banking, Financial Markets, Investment Banking, Corporate Finance,
                          Branch Banking, Business and Transaction Banking.</p>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </Container>
            <Container ref={(scrollFunc) => this.chart = scrollFunc}>
              <Row>
                <div className="col-lg-12">
                  <div className="table-box">
                    <CandleChart />
                  </div>
                </div>
              </Row>
            </Container>
            <Container>
              <Row>
                <div className="col-lg-12">
                  <div className="table-box volume-chart-box">
                    <CandleVolume />
                  </div>
                </div>
              </Row>
            </Container>
            {/* <ProfitAndLoss /> */}
            {/* BalanceSheet */}
            <BalanceSheet />

            {/* News And events */}
            <Container>
              <Row>
                <Col md="12">
                  <div className="profile-detail-box">
                    <Row>
                      <Col lg="6">
                        <div className="overview-box news-box">
                          <h5 className="table-heading">Recent News</h5>
                          <ul className="news-ul">
                            <li>
                              <img src="/images/yes-bank.jpg" width="" alt="Img" />
                              <a href="#">Market Movers: YES Bank shares extend rally; 381 stocks hit upper circuit <span>1 hour ago • Economic Times</span></a>
                            </li>
                            <li>
                              <img src="/images/yes-bank.jpg" width="" alt="Img" />
                              <a href="#">Market Movers: YES Bank shares extend rally; 381 stocks hit upper circuit <span>1 hour ago • Economic Times</span></a>
                            </li>
                            <li>
                              <img src="/images/yes-bank.jpg" width="" alt="Img" />
                              <a href="#">Market Movers: YES Bank shares extend rally; 381 stocks hit upper circuit <span>1 hour ago • Economic Times</span></a>
                            </li>
                          </ul>
                          <Link href="#"><a title="View All" className="news-view-all">View All</a></Link>
                        </div>
                      </Col>
                      <Col lg="6">
                        <div className="overview-box news-box">
                          <h5 className="table-heading">Recent Events</h5>
                          <ul className="news-ul events-ul">
                            <li>
                              <span className="event-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="" height="" fill="currentColor" class="bi bi-calendar2-check" viewBox="0 0 16 16">
                                  <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                                  <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                                  <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                                </svg>
                              </span>
                              <a href="#">Analysts/Institutional Investor Meet/Con. Call Updates <span>Announced OnDec 8, 2020</span>
                                <p>Yes Bank Limited has informed the Exchange about Credit Rating | Download</p>
                              </a>
                            </li>
                            <li>
                              <span className="event-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="" height="" fill="currentColor" class="bi bi-calendar2-check" viewBox="0 0 16 16">
                                  <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                                  <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                                  <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                                </svg>
                              </span>
                              <a href="#">Analysts/Institutional Investor Meet/Con. Call Updates <span>Announced OnDec 8, 2020</span>
                                <p>Yes Bank Limited has informed the Exchange about Credit Rating | Download</p>
                              </a>
                            </li>
                            <li>
                              <span className="event-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="" height="" fill="currentColor" class="bi bi-calendar2-check" viewBox="0 0 16 16">
                                  <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                                  <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                                  <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                                </svg>
                              </span>
                              <a href="#">Analysts/Institutional Investor Meet/Con. Call Updates <span>Announced OnDec 8, 2020</span>
                                <p>Yes Bank Limited has informed the Exchange about Credit Rating | Download</p>
                              </a>
                            </li>
                          </ul>
                          <Link href="#"><a title="View All" className="news-view-all">View All</a></Link>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
            </Container>
          </section>
        </section>
      </Layout >
    );
  }
}
export default ProfileDetail;