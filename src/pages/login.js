import Head from 'next/head'
import React, { Component } from 'react';
import Cookies from 'js-cookie';
import Layout from "../components/Layout/Layout";
import { Checkbox, TextField, Button, FormControlLabel } from '@material-ui/core';
import { Col, Container, Form, Row, } from 'react-bootstrap';
import Link from 'next/link'
import CloseIcon from '@material-ui/icons/Close';
import Router from 'next/router';
import SocialButton from 'components/socialButton';
import { AuthenticationService } from '_services/AuthenticationService';
import { encrypt } from '_helper/EncrDecrypt';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {},
      currentProvider: '',
      clickSocial: ''
    };
    this.nodes = {}
  }

  handleSocialLogin = (user) => {
    this.setState({
      clickSocial: user._provider
    });
    var login_flag = '';
    if (user._provider == 'facebook') {
      login_flag = 2;
    } else if (user._provider == 'google') {
      login_flag = 1;
    }
    var params = {
      mobile: '',
      first_name: '',
      last_name: '',
      email: '',
      login_flag: login_flag,
      social_token: user._token['accessToken']
    };
    if (user._profile) {
      if (user._profile['firstName']) {
        params['first_name'] = user._profile['firstName'];
      }
      if (user._profile['lastName']) {
        params['last_name'] = user._profile['lastName'];
      }
      if (user._profile['email']) {
        params['email'] = user._profile['email'];
      }
      if (user._profile['mobile']) {
        params['mobile'] = user._profile['mobile'];
      }
      AuthenticationService.signUp(params, (res) => {
        if (res.status) {
          var userJson = res['data'];
          var encryptData = encrypt(userJson);
          // Cookies.set('_accessToken', userData['token'], { expires: 30 });
          Cookies.set('_user', encryptData, { expires: 30 });
          AuthenticationService.setCurrentUserSubject(userJson);
          Router.push('/');
        } else {
          // this.setState({
          //   alert: {
          //     toast: true,
          //     toastMessage: res.message,
          //     severity: 'error'
          //   },
          // });
        }
      });
      // var value = '';
      // if (params['mobile'] && params['email']) {
      //   value = params['mobile'] + '-' + params['email'];
      // } else if (params['mobile']) {
      //   value = params['mobile'];
      // } else if (params['email']) {
      //   value = params['email'];
      // } else {
      //   value = '1';
      // }
    }
  }

  handleSocialLoginFailure = (err) => {
    // console.log(err);
    // if (this.state.clickSocial != '') {
    //   this.setState({
    //     alert: {
    //       toast: true,
    //       toastMessage: 'Something went wrong.',
    //       severity: 'error'
    //     },
    //   });
    // }
  }

  setNodeRef(provider, node) {
    if (node) {
      this.nodes[provider] = node
    }
  }

  render() {
    return (
      <section className="login-section">
        <Head>
          <title>Login | Stockathon</title>
        </Head>
        <Container>
          {/* Row Start */}
          <Row>
            <Col xs="12" lg="12" sm="12" md="12" className="text-center mb-5">
              <Link href="/">
                <a>
                  <img src="/images/logo.svg" width="300" />
                </a>
              </Link>
            </Col>
            <Col xs="12" lg="12" sm="12" md="12">
              <div className="login-box text-center">
                <Link href="/">
                  <a title="Close" className="close-login"> <CloseIcon /></a>
                </Link>
                {/* <img src="images/logo-icon.svg" width="50" /> */}
                <h2>Login / Register to Stockathon</h2>
                <p>Find your next great investment, now easier & faster with Stockathon</p>
                {/* <Link href="#"> */}
                <a title="Google">
                  <SocialButton
                    provider='google'
                    appId={process.env.GOOGLE_LOGIN_CLIENT_ID}
                    onLoginSuccess={this.handleSocialLogin}
                    onLoginFailure={this.handleSocialLoginFailure}
                    getInstance={this.setNodeRef.bind(this, 'google')}
                    key={'google'}
                    className="googlelogin"
                  >
                    <img src="/images/google.svg" width="44" /> Google

                        </SocialButton>
                </a>
                {/* </Link> */}
                {/* <Link href="#"> */}
                {/* <a title="Facebook">
                  <img src="/images/facebook.svg" width="44" /> Facebook
                </a> */}
                <a title="Facebook">
                  <SocialButton
                    provider='facebook'
                    appId={process.env.FACEBOOK_LOGIN_CLIENT_ID}
                    onLoginSuccess={this.handleSocialLogin}
                    onLoginFailure={this.handleSocialLoginFailure}
                    className="facebooklogin"
                    clicksocial={this.state.clickSocial}
                  >
                    <img src="/images/facebook.svg" width="44" /> Facebook

                       </SocialButton>
                </a>
                {/* </Link> */}
                <div className="open-acc-box">
                  <span>By signing you agree to the Terms and Conditions.</span>
                  {/* <Link href="#">
                    <a title="Open now">Open now</a>
                  </Link> */}
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    );
  }
}
export default Login;

{/* <Row className="text-center">
                <Col xs="12" lg="12" sm="12" md="12">
                  <div className="text-center mb-5">
                    <img src="images/logo.svg" width="300" />
                  </div>
                  <h2 className="mb-4">Sign In</h2>
                </Col>
                <Form className="m-auto">
                  <Col xs="12" lg="12" sm="12" md="12" className="login-input">
                    <TextField id="userName" label="Username" variant="outlined" size="small" />
                  </Col>
                  <Col xs="12" lg="12" sm="12" md="12" className="login-input">
                    <TextField
                      id="password"
                      label="Password"
                      type="password"
                      autoComplete="current-password"
                      variant="outlined"
                      size="small"
                    />
                  </Col>
                  <Col xs="12" lg="12" sm="12" md="12" className="login-input">
                  <FormControlLabel
                    control={
                      <Checkbox
                        name="check"
                        color="primary"
                      />
                    }
                    label="Keep me logged in"
                  />
                </Col>
                  <Col xs="12" lg="12" sm="12" md="12" className="my-4">
                    <Button variant="outlined" color="primary" className="w-100">
                      Log In
                       </Button>
                  </Col>
                </Form>
              </Row> */}