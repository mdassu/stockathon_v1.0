import Head from 'next/head'
import React, { Component } from 'react';
import { Button, Table, Container, Row, Col, Form } from 'react-bootstrap';
import Layout from "../components/Layout/Layout";
import { BookmarkIcon, CloudDownload } from '@material-ui/icons';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Link from 'next/link'
import BalanceSheet from '../components/Stock/BalanceSheet'
import ProfitAndLoss from '../components/Stock/ProfitAndLoss'
import Router from 'next/router';
import { StockService } from '_services/StockService';
import { CandleVolume } from 'components/Charts/CandleVolume';
import * as moment from 'moment';
import Loader from 'components/Common/Loader';
import * as Excel from "exceljs/dist/exceljs.min.js";
import * as fs from "file-saver";


class Stock extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stockData: {},
      pnlData: [],
      balanceSheetData: [],
      cashFlowData: [],
      chartData: [],
      pnlMainColumns: [],
      pnlColumns: [],
      pnlRows: [],
      balSheetMainColumns: [],
      balSheetColumns: [],
      balSheetRows: [],
      cashFlowMainColumns: [],
      cashFlowColumns: [],
      cashFlowRows: [],
      isLoaded: false,
      noData: false,
      isTableLoaded: false
    }
    this.scrollToElement = this.scrollToElement.bind(this);
  }

  static async getInitialProps(ctx) {
    return ctx.query;
  }

  componentDidMount() {
    this.getStockDetails();
    this.getChartData();
  }

  getStockDetails(type = '', tableType = '') {
    var params = {
      symbol: this.props.symbol,
      type: type,
      typeType: tableType
    };
    StockService.getStockDetails(params, res => {
      if (res.status) {
        var data = res.data;
        if (tableType == 'balsheet') {
          this.setState({
            balanceSheetData: data.balanceSheet,
            isTableLoaded: true
          });
        } else {
          this.setState({
            stockData: data.description,
            pnlData: data.pnl,
            balanceSheetData: data.balanceSheet,
            cashFlowData: data.cashflow,
            isLoaded: true,
            isTableLoaded: true
          });
        }
        this.manageExportData(this.state.pnlData, this.state.balanceSheetData, this.state.cashFlowData);
      } else {
        this.setState({
          noData: true,
          isLoaded: true
        })
      }
    });
  }

  getChartData() {
    var params = {
      symbol: this.props.symbol
    };
    StockService.getChartData(params, res => {
      if (res.status) {
        this.setState({
          chartData: res['data']
        });
      } else {
        this.setState({
          chartData: []
        });
      }
    });
  }

  manageExportData = (pnlData, balSheetData, cashFlowData) => {
    var pnlMainColumns = [];
    var pnlColumns = [];
    var pnlRows = [];
    var balSheetMainColumns = [];
    var balSheetColumns = [];
    var balSheetRows = [];
    var cashFlowMainColumns = [];
    var cashFlowColumns = [];
    var cashFlowRows = [];
    var firstColumn = ['Narration'];

    // Profit & Loss
    if (pnlData && pnlData.length > 0) {
      var pnlHead = Object.keys(pnlData[0]);
      if (pnlHead.indexOf('innerData') != -1) {
        pnlHead.splice(pnlHead.indexOf('innerData'), 1);
      }
      if (pnlHead.indexOf('type') != -1) {
        pnlHead.splice(pnlHead.indexOf('type'), 1);
      }
      if (pnlHead.indexOf('id') != -1) {
        pnlHead.splice(pnlHead.indexOf('id'), 1);
      }
      if (pnlHead.indexOf('isTotal') != -1) {
        pnlHead.splice(pnlHead.indexOf('isTotal'), 1);
      }
      pnlColumns = firstColumn.concat(pnlHead);

      pnlData.map((data, index) => {
        var dataJson = {};
        pnlColumns.map((item, key) => {
          if (key == 0) {
            dataJson[item] = data['type'];
            if (index == 0) {
              pnlMainColumns.push(this.state.stockData['stock_name']);
            }
          } else {
            dataJson[item] = data[item];
            if (pnlColumns.length - 1 == key && index == 0) {
              pnlMainColumns.push('stockathon.com');
            } else if (index == 0) {
              pnlMainColumns.push('');
            }
          }

        });
        pnlRows.push(dataJson);
      });
    }

    if (balSheetData && balSheetData.length > 0) {

      // Balance Sheet
      var balSheetHead = Object.keys(balSheetData[0]);
      if (balSheetHead.indexOf('innerData') != -1) {
        balSheetHead.splice(balSheetHead.indexOf('innerData'), 1);
      }
      if (balSheetHead.indexOf('type') != -1) {
        balSheetHead.splice(balSheetHead.indexOf('type'), 1);
      }
      if (balSheetHead.indexOf('id') != -1) {
        balSheetHead.splice(balSheetHead.indexOf('id'), 1);
      }
      if (balSheetHead.indexOf('isTotal') != -1) {
        balSheetHead.splice(balSheetHead.indexOf('isTotal'), 1);
      }
      balSheetColumns = firstColumn.concat(balSheetHead);

      balSheetData.map((data, index) => {
        var dataJson = {};
        balSheetColumns.map((item, key) => {
          if (key == 0) {
            dataJson[item] = data['type'];
            if (index == 0) {
              balSheetMainColumns.push(this.state.stockData['stock_name']);
            }
          } else {
            dataJson[item] = data[item];
            if (balSheetColumns.length - 1 == key && index == 0) {
              balSheetMainColumns.push('stockathon.com');
            } else if (index == 0) {
              balSheetMainColumns.push('');
            }
          }

        });
        balSheetRows.push(dataJson);
      });
    }

    if (cashFlowData && cashFlowData.length > 0) {

      // Cash Flow
      var cashFlowHead = Object.keys(cashFlowData[0]);
      if (cashFlowHead.indexOf('innerData') != -1) {
        cashFlowHead.splice(cashFlowHead.indexOf('innerData'), 1);
      }
      if (cashFlowHead.indexOf('type') != -1) {
        cashFlowHead.splice(cashFlowHead.indexOf('type'), 1);
      }
      if (cashFlowHead.indexOf('id') != -1) {
        cashFlowHead.splice(cashFlowHead.indexOf('id'), 1);
      }
      if (cashFlowHead.indexOf('isTotal') != -1) {
        cashFlowHead.splice(cashFlowHead.indexOf('isTotal'), 1);
      }
      cashFlowColumns = firstColumn.concat(cashFlowHead);

      cashFlowData.map((data, index) => {
        var dataJson = {};
        cashFlowColumns.map((item, key) => {
          if (key == 0) {
            dataJson[item] = data['type'];
            if (index == 0) {
              cashFlowMainColumns.push(this.state.stockData['stock_name']);
            }
          } else {
            dataJson[item] = data[item];
            if (cashFlowColumns.length - 1 == key && index == 0) {
              cashFlowMainColumns.push('stockathon.com');
            } else if (index == 0) {
              cashFlowMainColumns.push('');
            }
          }

        });
        cashFlowRows.push(dataJson);
      });
    }

    this.setState({
      pnlMainColumns: pnlMainColumns,
      pnlColumns: pnlColumns,
      pnlRows: pnlRows,
      balSheetMainColumns: balSheetMainColumns,
      balSheetColumns: balSheetColumns,
      balSheetRows: balSheetRows,
      cashFlowMainColumns: cashFlowMainColumns,
      cashFlowColumns: cashFlowColumns,
      cashFlowRows: cashFlowRows,
    });
  }

  scrollToElement(scrollFunc) {
    scrollFunc.scrollIntoView({ behavior: 'smooth' });
  }

  exportData = () => {
    const fileName = this.state.stockData['stock_name'];
    const EXCEL_EXTENSION = '.xlsx';
    const EXCEL_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    let workbook = new Excel.Workbook();

    // Profit & Loss
    let pnlWorksheet = workbook.addWorksheet("Profit & Loss");

    pnlWorksheet.addRow(this.state.pnlMainColumns);
    pnlWorksheet.addRow([]);

    let pnlHeaderRow = pnlWorksheet.addRow(this.state.pnlColumns);
    // Cell Style : Fill and Border
    pnlHeaderRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: '1C2161' },
      };
      cell.font = {
        color: { argb: 'FFFFFF' },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    this.state.pnlRows.map((element) => {
      let eachRow = [];
      this.state.pnlColumns.map((headers, key) => {
        eachRow.push(element[headers]);
      });
      pnlWorksheet.addRow(eachRow);
    });

    // Balance Sheet
    let balSheetWorksheet = workbook.addWorksheet("Balance Sheet");

    balSheetWorksheet.addRow(this.state.balSheetMainColumns);
    balSheetWorksheet.addRow([]);

    let balSheetHeaderRow = balSheetWorksheet.addRow(this.state.balSheetColumns);
    // Cell Style : Fill and Border
    balSheetHeaderRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: '1C2161' },
      };
      cell.font = {
        color: { argb: 'FFFFFF' },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    this.state.balSheetRows.map((element) => {
      let eachRow = [];
      this.state.balSheetColumns.map((headers, key) => {
        eachRow.push(element[headers]);
      });
      balSheetWorksheet.addRow(eachRow);
    });

    // Cash Flow
    let cashFlowWorksheet = workbook.addWorksheet("Cash Flow");

    cashFlowWorksheet.addRow(this.state.cashFlowMainColumns);
    cashFlowWorksheet.addRow([]);

    let cashFlowHeaderRow = cashFlowWorksheet.addRow(this.state.cashFlowColumns);
    // Cell Style : Fill and Border
    cashFlowHeaderRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: '1C2161' },
      };
      cell.font = {
        color: { argb: 'FFFFFF' },
      };
      cell.border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    });
    this.state.cashFlowRows.map((element) => {
      let eachRow = [];
      this.state.cashFlowColumns.map((headers, key) => {
        eachRow.push(element[headers]);
      });
      cashFlowWorksheet.addRow(eachRow);
    });

    // worksheet.addRow([]);
    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: EXCEL_TYPE });
      fs.saveAs(
        blob,
        fileName + EXCEL_EXTENSION
      );
    });
  }

  changeTableView = (e, tableType) => {

    if (tableType == 'balsheet') {
      this.setState({
        balanceSheetData: [],
        balSheetMainColumns: [],
        balSheetColumns: [],
        balSheetRows: [],
        isTableLoaded: false
      }, () => {
        this.getStockDetails(e.target.value, tableType);
      });
    }
  }

  render() {

    const { stockData, chartData, pnlData, balanceSheetData, cashFlowData, isLoaded, isTableLoaded, noData } = this.state;

    return (
      <Layout title={`${stockData['stock_name']} share price | ${stockData['stock_name']} Technical Analysis | Stockathon`}>
        {
          isLoaded ?
            Object.keys(stockData) && Object.keys(stockData).length > 0 ? (
              <section className="profile-main-section">
                <section className="profile-banner-section">
                  <Container>
                    {/* Row Start */}
                    <Row>
                      <Col lg="12" className="py-4">
                        <Row>
                          <Col lg="6" md="6">
                            <div className="banner-heading">
                              <h1>{stockData.stock_name} <span>{stockData.symbol}</span></h1>
                              <h2>{stockData.close} <span className={stockData.change < 0 ? 'low' : 'high'}>
                                {
                                  stockData.change < 0 ? (
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-down-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                      <path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
                                    </svg>
                                  ) : (
                                      <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-caret-up-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M7.247 4.86l-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z" />
                                      </svg>
                                    )
                                }
                                {stockData.change} ({stockData.changeper}%)</span></h2>
                              {/* <h2>14.35 <span className="low"> 0.20 (1.41%)</span></h2> */}
                            </div>
                          </Col>
                          <Col lg="6" md="6">
                            <div className="banner-btns">
                              {/* <Button className="save-alert" title="Save Alert" onClick={this.exportData}><NotificationsIcon /> Save Alert</Button>
                              <Button className="watchlist-btn" title="Add To Watchlist"><BookmarkIcon /> Add To Watchlist</Button> */}
                              <Button className="watchlist-btn" title="Export to Excel" onClick={this.exportData}><CloudDownload /> Export to Excel</Button>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg="12">
                        <div className="profile-ratio">
                          <ul>
                            <li>
                              <h5>Sector</h5>
                              <span>{stockData['gsector']}</span>
                            </li>
                            <li>
                              <h5>Industry</h5>
                              <span>{stockData['finnhubIndustry']}</span>
                            </li>
                            <li>
                              <h5>Sub Industry</h5>
                              <span>{stockData['gsubind']}</span>
                            </li>
                            <li>
                              <h5># of Employees</h5>
                              <span>{stockData['employeeTotal']}</span>
                            </li>
                            <li>
                              <h5>IPO Date</h5>
                              <span>{moment(stockData['ipo']).format('YYYY-MM-DD')}</span>
                            </li>
                          </ul>
                        </div>
                      </Col>

                    </Row>
                  </Container>
                </section>
                <section className="sticky">
                  <Container>
                    <Row>
                      <Col lg="12">
                        <div className="profile-tabs table-responsive">
                          <ul>
                            <li>
                              <a title="Overview" href="#overview" className="active-tab" onClick={() => this.scrollToElement(this.overview)}>Overview</a>
                            </li>
                            <li>
                              <a title="Chart" onClick={() => this.scrollToElement(this.chart)}>Chart</a>
                            </li>
                            {/* <li>
                              <a title="Analysis">Analysis</a>
                            </li>
                            <li>
                              <a title="Peers">Peers</a>
                            </li>
                            <li>
                              <a title="Quarters">Quarters</a>
                            </li> */}
                            <li>
                              <a title="Profile & Loss" onClick={() => this.scrollToElement(this.pnl)}>Profile & Loss</a>
                            </li>
                            <li>
                              <a title="Balance Sheet" onClick={() => this.scrollToElement(this.bsheet)}>Balance Sheet</a>
                            </li>
                            <li>
                              <a title="Cash Flow" onClick={() => this.scrollToElement(this.cflow)}>Cash Flow</a>
                            </li>
                            {/* <li>
                              <a title="Ratios">Ratios</a>
                            </li> */}
                          </ul>
                        </div>
                      </Col>
                    </Row>
                  </Container>
                </section>
                <section className="profile-content-section">
                  {/* Overview */}
                  <Container ref={(scrollFunc) => this.overview = scrollFunc}>
                    <Row>
                      <Col md="12">
                        <div className="profile-detail-box">
                          <Row>
                            <Col lg="6">
                              <div className="overview-box">
                                <h5 className="table-heading">Overview</h5>
                                <ul className="overview-ul">
                                  <li>
                                    <Link href="#"><a>Market cap <span>US${stockData['marketCapitalization']}</span></a></Link>
                                  </li>
                                  <li>
                                    <Link href="#"><a>52 Week Range <span>{`${stockData['_52WeekLow']} ~ ${stockData['_52WeekHigh']}`}</span></a></Link>
                                  </li>
                                  <li className="active-row">
                                    <Link href="#"><a>Beta <span>{stockData['beta']}</span></a></Link>
                                  </li>
                                  <li className="active-row">
                                    <Link href="#"><a>P/E <span>{stockData['peNormalizedAnnual']}</span></a></Link>
                                  </li>
                                  <li>
                                    <Link href="#"><a>P/B <span>{stockData['pbAnnual']}</span></a></Link>
                                  </li>
                                  <li>
                                    <Link href="#"><a>ROI <span>{stockData['roiAnnual']}</span></a></Link>
                                  </li>
                                  <li className="active-row">
                                    <Link href="#"><a>Dividend Yield <span>{stockData['currentDividendYieldTTM']}</span></a></Link>
                                  </li>
                                  <li className="active-row">
                                    <Link href="#"><a>Current Ratio <span>{stockData['currentRatioAnnual']}</span></a></Link>
                                  </li>
                                  <li className="active-row">
                                    <Link href="#"><a>Quick Ratio <span>{stockData['quickRatioAnnual']}</span></a></Link>
                                  </li>
                                  <li className="active-row">
                                    <Link href="#"><a>Share Outstanding <span>{stockData['shareOutstanding']}</span></a></Link>
                                  </li>
                                </ul>
                              </div>
                            </Col>
                            <Col lg="6">
                              <div className="overview-box">
                                <h5 className="table-heading">Profile</h5>
                                <p>{stockData['company_description']}</p>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                    </Row>
                  </Container>
                  {/* ProfitAndLoss */}
                  {
                    chartData.length > 0 ? (
                      <Container ref={(scrollFunc) => this.chart = scrollFunc}>
                        <Row>
                          <div className="col-lg-12">
                            <div className="table-box  volume-chart-box">
                              <CandleVolume chartData={chartData} />
                            </div>
                          </div>
                        </Row>
                      </Container>
                    ) : ''
                  }
                  {
                    !noData ? (
                      <React.Fragment>
                        <Container ref={(scrollFunc) => this.pnl = scrollFunc}>
                          <Row>
                            <Col lg="12">
                              <div className="table-box">
                                <Row>
                                  <Col md="6">
                                    <h5 className="table-heading">Profit & Loss</h5>
                                    <p className="table-p">Consolidated Figures in Rs. Crores / <Link href="#"><a>View Standalone</a></Link> </p>
                                  </Col>
                                  {/* <Col md="6">
                                  <Button className="table-button" title="Product Segments">Product Segments</Button>
                                </Col> */}
                                </Row>
                                {pnlData && pnlData.length > 0 ? (<ProfitAndLoss data={pnlData} />) : ''}
                              </div>
                            </Col>
                          </Row>
                        </Container>
                        {/* BalanceSheet */}

                        <Container ref={(scrollFunc) => this.bsheet = scrollFunc}>
                          <Row>
                            <Col md="12">
                              <div className="table-box">
                                <Row>
                                  <Col md="6">
                                    <h5 className="table-heading">Balance Sheet</h5>
                                    <p className="table-p">Consolidated Figures in Rs. Crores / <Link href="#"><a>View Standalone</a></Link> </p>
                                  </Col>
                                  <Col md="6">
                                    {/* <Button className="table-button" title="Corporate Actions">Corporate Actions</Button> */}
                                    <div className="table-view">
                                      <Form.Label>Table View</Form.Label>
                                      <Form.Control as="select" onChange={($event) => this.changeTableView($event, 'balsheet')}>
                                        <option value="">Annually</option>
                                        <option value="quarterly">Quarterly</option>
                                      </Form.Control>
                                    </div>
                                  </Col>
                                </Row>
                                {isTableLoaded ? balanceSheetData && balanceSheetData.length > 0 ? (<ProfitAndLoss data={balanceSheetData} />) : '' : <Loader />}
                              </div>
                            </Col>
                          </Row>
                        </Container>


                        {/* <BalanceSheet /> */}

                        {/* Cash Flows */}
                        <Container ref={(scrollFunc) => this.cflow = scrollFunc}>
                          <Row>
                            <Col md="12">
                              <div className="table-box">
                                <Row>
                                  <Col md="6">
                                    <h5 className="table-heading">Cash Flows</h5>
                                    <p className="table-p">Consolidated Figures in Rs. Crores / <Link href="#"><a>View Standalone</a></Link> </p>
                                  </Col>
                                </Row>
                                {cashFlowData && cashFlowData.length > 0 ? (<ProfitAndLoss data={cashFlowData} />) : ''}
                              </div>
                            </Col>
                          </Row>
                        </Container>
                        {/* News And events */}
                        <Container>
                          <Row>
                            <Col md="12">
                              <div className="profile-detail-box">
                                <Row>
                                  <Col lg="6">
                                    <div className="overview-box news-box">
                                      <h5 className="table-heading">Recent News</h5>
                                      <ul className="news-ul">
                                        <li>
                                          <img src="/images/yes-bank.jpg" width="" alt="Img" />
                                          <a href="#">Market Movers: YES Bank shares extend rally; 381 stocks hit upper circuit <span>1 hour ago • Economic Times</span></a>
                                        </li>
                                        <li>
                                          <img src="/images/yes-bank.jpg" width="" alt="Img" />
                                          <a href="#">Market Movers: YES Bank shares extend rally; 381 stocks hit upper circuit <span>1 hour ago • Economic Times</span></a>
                                        </li>
                                        <li>
                                          <img src="/images/yes-bank.jpg" width="" alt="Img" />
                                          <a href="#">Market Movers: YES Bank shares extend rally; 381 stocks hit upper circuit <span>1 hour ago • Economic Times</span></a>
                                        </li>
                                      </ul>
                                      <Link href="#"><a title="View All" className="news-view-all">View All</a></Link>
                                    </div>
                                  </Col>
                                  <Col lg="6">
                                    <div className="overview-box news-box">
                                      <h5 className="table-heading">Recent Events</h5>
                                      <ul className="news-ul events-ul">
                                        <li>
                                          <span className="event-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="" height="" fill="currentColor" class="bi bi-calendar2-check" viewBox="0 0 16 16">
                                              <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                                              <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                                              <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                                            </svg>
                                          </span>
                                          <a href="#">Analysts/Institutional Investor Meet/Con. Call Updates <span>Announced OnDec 8, 2020</span>
                                            <p>Yes Bank Limited has informed the Exchange about Credit Rating | Download</p>
                                          </a>
                                        </li>
                                        <li>
                                          <span className="event-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="" height="" fill="currentColor" class="bi bi-calendar2-check" viewBox="0 0 16 16">
                                              <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                                              <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                                              <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                                            </svg>
                                          </span>
                                          <a href="#">Analysts/Institutional Investor Meet/Con. Call Updates <span>Announced OnDec 8, 2020</span>
                                            <p>Yes Bank Limited has informed the Exchange about Credit Rating | Download</p>
                                          </a>
                                        </li>
                                        <li>
                                          <span className="event-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="" height="" fill="currentColor" class="bi bi-calendar2-check" viewBox="0 0 16 16">
                                              <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z" />
                                              <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z" />
                                              <path fill-rule="evenodd" d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                                            </svg>
                                          </span>
                                          <a href="#">Analysts/Institutional Investor Meet/Con. Call Updates <span>Announced OnDec 8, 2020</span>
                                            <p>Yes Bank Limited has informed the Exchange about Credit Rating | Download</p>
                                          </a>
                                        </li>
                                      </ul>
                                      <Link href="#"><a title="View All" className="news-view-all">View All</a></Link>
                                    </div>
                                  </Col>
                                </Row>
                              </div>
                            </Col>
                          </Row>
                        </Container>
                      </React.Fragment>
                    ) : ''
                  }
                </section>
              </section>
            ) : (
                <Loader />
              )
            : (
              <Loader />
            )
        }
      </Layout >
    );
  }
}
export default Stock;