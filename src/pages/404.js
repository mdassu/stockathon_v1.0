import Head from 'next/head'
import React, { Component } from 'react';
import Layout from "../components/Layout/Layout";
import { Container, Row, Col } from 'react-bootstrap';
import Link from 'next/link'

class ErrorPage extends React.Component {
  render() {
    return (
      <Layout>
        <section>
          <Container>
            <Row>
              <Col md="12">
                <div className="error-box">
                  <img src="/images/404.svg" width="" alt="Img" />
                  <h1>Error 404: Page Not Found</h1>
                  <p>We couldn't find the page you are looking for.</p>
                  <Link href="/"><a className="back-btn" title="Back To Home">Back To Home</a></Link>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </Layout >
    );
  }
}
export default ErrorPage;