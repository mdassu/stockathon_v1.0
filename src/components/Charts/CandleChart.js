import React, { Component, Fragment } from 'react';
import Router from 'next/router';
import { Button, Table, Container, Row, Col } from 'react-bootstrap';
import { ChartComponent, SeriesCollectionDirective, RowsDirective, RowDirective, AxesDirective, AxisDirective, SeriesDirective, Inject, ColumnSeries, Legend, Category, Tooltip, DataLabel, LineSeries, CandleSeries, Crosshair, Zoom } from '@syncfusion/ej2-react-charts';

import Form from 'react-bootstrap/Form';
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';

class CandleChart extends Component {

  constructor(props) {
    super(props);
    this.state = {
      stockList: [],
      selected: '',
      chartType: 'candle'
    };
    this.data = [
      { x: 'Jan', y: 15, y1: 33 }, { x: 'Feb', y: 20, y1: 31 }, { x: 'Mar', y: 35, y1: 30 },
      { x: 'Apr', y: 40, y1: 28 }, { x: 'May', y: 80, y1: 29 }, { x: 'Jun', y: 70, y1: 30 },
      { x: 'Jul', y: 65, y1: 33 }, { x: 'Aug', y: 55, y1: 32 }, { x: 'Sep', y: 50, y1: 34 },
      { x: 'Oct', y: 30, y1: 32 }, { x: 'Nov', y: 35, y1: 32 }, { x: 'Dec', y: 35, y1: 31 }
    ];
    this.chartData = [
      { x: 'Jan', open: 1200, high: 1600, low: 1000, close: 1400, volume: 5000 },
      { x: 'Feb', open: 1500, high: 1900, low: 1300, close: 1700, volume: 4502 },
      { x: 'Mar', open: 1300, high: 1700, low: 1100, close: 1500, volume: 4150 },
      { x: 'Apr', open: 1600, high: 1800, low: 1200, close: 1400, volume: 3302 },
      { x: 'May', open: 1500, high: 1700, low: 1100, close: 1300, volume: 4400 },
      { x: 'Jun', open: 1200, high: 1600, low: 1000, close: 1400, volume: 3250 },
      { x: 'Jul', open: 1500, high: 1900, low: 1300, close: 1700, volume: 3950 },
      { x: 'Aug', open: 1300, high: 1700, low: 1100, close: 1500, volume: 3852 },
      { x: 'Sep', open: 1600, high: 1800, low: 1200, close: 1400, volume: 3574 },
      { x: 'Oct', open: 1500, high: 1700, low: 1100, close: 1300, volume: 4520 },
      { x: 'Nov', open: 1200, high: 1600, low: 1000, close: 1400, volume: 4320 },
      { x: 'Dec', open: 1500, high: 1900, low: 1300, close: 1700, volume: 4000 }
    ];
    // this.pxAxis = { valueType: 'Category', title: 'Months', interval: 1 };
    this.pyAxis = {
      title: 'Volume',
      lineStyle: { width: 0 }
    };
    this.lines = { width: 0 };
    this.marker = { visible: true, width: 10, height: 10, border: { width: 2, color: '#F8AB1D' } };
    this.primaryxAxis = { valueType: 'Category', majorGridLines: { width: 0 } };
    this.style = { textAlign: "center" };
    this.legendSettings = { visible: true };
  }

  componentDidMount() {
    // this.getStockData();
  }



  render() {
    return (
      <React.Fragment>
        <Row>
          <Col md="4">
            <div className="chart-range">
              <ul>
                <li>
                  <a href="#">1m</a>
                </li>
                <li>
                  <a href="#">3m</a>
                </li>
                <li>
                  <a href="#">6m</a>
                </li>
                <li>
                  <a href="#" className="active">1Yr</a>
                </li>
                <li>
                  <a href="#">3Yr</a>
                </li>
                <li>
                  <a href="#">5Yr</a>
                </li>
                <li>
                  <a href="#">Max</a>
                </li>
              </ul>
            </div>
          </Col>
          <Col md="4">
            <div className="chart-range float-right">
              <ul>
                <li>
                  <a className="cursor" onClick={() => this.setState({ chartType: 'candle' })}>Candle</a>
                </li>
                <li>
                  <a className="cursor" onClick={() => this.setState({ chartType: 'line' })}>Line</a>
                </li>
              </ul>
            </div>
          </Col>
          <Col md="4">
            <div className="chart-range float-right">
              <ul>
                <li>
                  <a href="#">Daily</a>
                </li>
                <li>
                  <a href="#">Weekly</a>
                </li>
                <li>
                  <a href="#" className="active">Monthly</a>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
        <ChartComponent id='charts' style={this.style} primaryXAxis={this.primaryxAxis} primaryYAxis={this.pyAxis} legendSettings={this.legendSettings} title='Weather Condition'>
          <Inject services={[ColumnSeries, CandleSeries, LineSeries, Legend, Tooltip, DataLabel, Category]} />
          <AxesDirective>
            {/* <AxisDirective rowIndex={1} name='yAxis1' opposedPosition={true} title='Temperature (Celsius)' labelFormat='{value}°C' minimum={24} maximum={36} interval={2} majorGridLines={this.lines} lineStyle={this.lines}>
            </AxisDirective> */}
            <AxisDirective rowIndex={1} name='yAxis1' opposedPosition={true} title='Price'>
            </AxisDirective>
          </AxesDirective>
          <RowsDirective>
            <RowDirective height='50%'></RowDirective>
            <RowDirective height='50%'></RowDirective>
          </RowsDirective>
          <SeriesCollectionDirective>
            {
              this.state.chartType == 'line' ? (
                <SeriesDirective dataSource={this.chartData} xName='x' yName='close' name='Close Price' type='Line' marker={this.marker} yAxisName='yAxis1'>
                </SeriesDirective>
              ) : (
                  <SeriesDirective dataSource={this.chartData} xName='x' yName='close' name='Candle' type='Candle' low='low' high='high' open='open' close='close' yAxisName='yAxis1'>
                  </SeriesDirective>
                )
            }

            {/*  */}
            <SeriesDirective dataSource={this.chartData} xName='x' yName='volume' name='Volume' type='Column'>
            </SeriesDirective>
          </SeriesCollectionDirective>
        </ChartComponent>
        {/* <ChartComponent id='chartss' style={this.style} primaryXAxis={this.primaryxAxis} primaryYAxis={this.primaryyAxis} legendSettings={this.legendSettings} title='Shirpur Gold Refinery Share Price'>
          <Inject services={[CandleSeries, Tooltip, Category, Crosshair, Zoom]} />
          <SeriesCollectionDirective>
            <SeriesDirective dataSource={this.chartData} xName='x' yName='low' name='SHIRPUR-G' type='Candle' low='low' high='high' open='open' close='close'>
            </SeriesDirective>
          </SeriesCollectionDirective>
        </ChartComponent> */}
      </React.Fragment >
    );
  }
}

export default CandleChart;