import * as React from "react";
import { StockChartComponent, StockChartSeriesCollectionDirective, StockChartSeriesDirective, Inject, Crosshair, DateTime, Tooltip, RangeTooltip, ColumnSeries, LineSeries, SplineSeries, CandleSeries, HiloOpenCloseSeries, HiloSeries, RangeAreaSeries, Trendlines, StockChartRowsDirective, StockChartRowDirective, StockChartAxesDirective, StockChartAxisDirective } from '@syncfusion/ej2-react-charts';
import { EmaIndicator, RsiIndicator, BollingerBands, TmaIndicator, MomentumIndicator, SmaIndicator, AtrIndicator, AccumulationDistributionIndicator, MacdIndicator, StochasticIndicator } from '@syncfusion/ej2-react-charts';
import { chartData } from './indicator-data';
import { StockService } from "_services/StockService";
const SAMPLE_CSS = `
    .control-fluid {
        padding: 0px !important;
    }
        .charts {
            align :center
        }`;
export let tooltipRender = (args) => {
  if (args.text.split('<br/>')[4]) {
    let target = parseInt(args.text.split('<br/>')[4].split('<b>')[1].split('</b>')[0]);
    let value = (target / 100000000).toFixed(1) + 'B';
    args.text = args.text.replace(args.text.split('<br/>')[4].split('<b>')[1].split('</b>')[0], value);
  }
};
export class CandleVolume extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (<div className='control-pane'>
      <style>
        {SAMPLE_CSS}
      </style>
      <div className='control-section'>
        <StockChartComponent
          id='stockchartpane'
          primaryYAxis={{
            lineStyle: { color: '#2b2b43', width: 0 },
            majorTickLines: { color: '#2b2b43', width: 1 },
            majorGridLines: { color: '#e8e9eb', width: .8 },
            labelPosition: 'outside',
            opposedPosition: true,
            labelStyle: {
              color: 'red',
              size: 15
            },
            title: 'Volume',
          }}
          primaryXAxis={{
            lineStyle: { color: '#2b2b43', width: 0 },
            majorTickLines: { color: '#2b2b43', width: 1 },
            crosshairTooltip: { enable: true },
            majorGridLines: { width: 0 },
            valueType: 'DateTime',
          }}
          chartArea={{ border: { width: 0 } }}
          tooltip={{ enable: true }}
          tooltipRender={tooltipRender}
          axisLabelRender={this.axisLabelRender.bind(this)}
          crosshair={{ enable: true }}
          load={this.load.bind(this)}
          indicatorType={[]}
          trendlineType={[]}
          seriesType={['Line', 'Candle']}
          enableCustomRange={false}
          enableSelector={false}
          periods={[
            { text: '1M', interval: 1, intervalType: 'Months' },
            { text: '3M', interval: 3, intervalType: 'Months' },
            { text: '6M', interval: 6, intervalType: 'Months' },
            { text: '1Y', interval: 1, intervalType: 'Years', selected: true },
            { text: '3Y', interval: 3, intervalType: 'Years' },
            { text: '5Y', interval: 5, intervalType: 'Years' }, { text: 'All' }
          ]}
        >
          <Inject services={[DateTime, Crosshair, Tooltip, RangeTooltip, ColumnSeries, LineSeries, CandleSeries]} />
          <StockChartRowsDirective>
            <StockChartRowDirective height={'25%'}>
            </StockChartRowDirective>
            <StockChartRowDirective height={'10%'}>
            </StockChartRowDirective>
            <StockChartRowDirective height={'65%'}>
            </StockChartRowDirective>
          </StockChartRowsDirective>
          <StockChartAxesDirective>
            <StockChartAxisDirective name='yAxis1' rowIndex={2} title="Price" opposedPosition={true} lineStyle={{ color: '#2b2b43', width: 0 }} majorGridLines={{ color: '#e8e9eb', width: .8 }} majorTickLines={{ color: '#2b2b43', width: 1 }}>
            </StockChartAxisDirective>
          </StockChartAxesDirective>
          <StockChartSeriesCollectionDirective>
            <StockChartSeriesDirective dataSource={this.props.chartData} xName='date' yName='volume' type='Column' fill="#BED3FD">
            </StockChartSeriesDirective>
            <StockChartSeriesDirective dataSource={this.props.chartData} xName='date' yName='close' type='Line' yAxisName='yAxis1' fill="#645DF9">
            </StockChartSeriesDirective>
          </StockChartSeriesCollectionDirective>
        </StockChartComponent>
      </div>
    </div >);
  }
  axisLabelRender(args) {
    let text = parseInt(args.text);
    if (args.axis.name === "primaryYAxis") {
      args.text = text / 100000000 + 'B';
    }
  }
  load(args) {
    let selectedTheme = location.hash.split('/')[1];
    selectedTheme = selectedTheme ? selectedTheme : 'Material';
    args.stockChart.theme = (selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).
      replace(/-dark/i, "Dark");
  }
}