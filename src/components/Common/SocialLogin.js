import Head from 'next/head'
import React, { Component } from 'react';
import Cookies from 'js-cookie';
import Layout from "components/Layout/Layout";
import { Checkbox, TextField, Button, FormControlLabel } from '@material-ui/core';
import { Col, Container, Form, Row, } from 'react-bootstrap';
import Link from 'next/link'
import CloseIcon from '@material-ui/icons/Close';
import Router from 'next/router';
import SocialButton from 'components/socialButton';
import { AuthenticationService } from '_services/AuthenticationService';
import { encrypt } from '_helper/EncrDecrypt';

class SocialLogin extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {},
      currentProvider: '',
      clickSocial: ''
    };
    this.nodes = {}
  }

  handleSocialLogin = (user) => {
    this.setState({
      clickSocial: user._provider
    });
    var login_flag = '';
    if (user._provider == 'facebook') {
      login_flag = 2;
    } else if (user._provider == 'google') {
      login_flag = 1;
    }
    var params = {
      mobile: '',
      first_name: '',
      last_name: '',
      email: '',
      login_flag: login_flag,
      social_token: user._token['accessToken']
    };
    if (user._profile) {
      if (user._profile['firstName']) {
        params['first_name'] = user._profile['firstName'];
      }
      if (user._profile['lastName']) {
        params['last_name'] = user._profile['lastName'];
      }
      if (user._profile['email']) {
        params['email'] = user._profile['email'];
      }
      if (user._profile['mobile']) {
        params['mobile'] = user._profile['mobile'];
      }
      AuthenticationService.signUp(params, (res) => {
        if (res.status) {
          var userJson = res['data'];
          var encryptData = encrypt(userJson);
          // Cookies.set('_accessToken', userData['token'], { expires: 30 });
          Cookies.set('_user', encryptData, { expires: 30 });
          AuthenticationService.setCurrentUserSubject(userJson);
          // Router.push('/');
          this.props.hidePopup();
        } else {
        }
      });
    }
  }

  handleSocialLoginFailure = (err) => {
    // console.log(err);
    // if (this.state.clickSocial != '') {
    //   this.setState({
    //     alert: {
    //       toast: true,
    //       toastMessage: 'Something went wrong.',
    //       severity: 'error'
    //     },
    //   });
    // }
  }

  setNodeRef(provider, node) {
    if (node) {
      this.nodes[provider] = node
    }
  }

  render() {
    return (
      <div className="login-box text-center">
        <h2><svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M7.938 2.016a.146.146 0 0 0-.054.057L1.027 13.74a.176.176 0 0 0-.002.183c.016.03.037.05.054.06.015.01.034.017.066.017h13.713a.12.12 0 0 0 .066-.017.163.163 0 0 0 .055-.06.176.176 0 0 0-.003-.183L8.12 2.073a.146.146 0 0 0-.054-.057A.13.13 0 0 0 8.002 2a.13.13 0 0 0-.064.016zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z" />
          <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z" />
        </svg> You need to login to access this feature</h2>
        <p>Find your next great investment, now easier & faster with Stockathon</p>
        <a title="Google">
          <SocialButton
            provider='google'
            appId={process.env.GOOGLE_LOGIN_CLIENT_ID}
            onLoginSuccess={this.handleSocialLogin}
            onLoginFailure={this.handleSocialLoginFailure}
            getInstance={this.setNodeRef.bind(this, 'google')}
            key={'google'}
            scope={'https://www.googleapis.com/auth/user.gender.read'}
            className="googlelogin"
          >
            <img src="/images/google.svg" width="44" /> Google
          </SocialButton>
        </a>
        <a title="Facebook">
          <SocialButton
            provider='facebook'
            appId={process.env.FACEBOOK_LOGIN_CLIENT_ID}
            onLoginSuccess={this.handleSocialLogin}
            onLoginFailure={this.handleSocialLoginFailure}
            className="facebooklogin"
            clicksocial={this.state.clickSocial}
          >
            <img src="/images/facebook.svg" width="44" /> Facebook
          </SocialButton>
        </a>
        <div className="open-acc-box">
          <span>By signing you agree to the Terms and Conditions.</span>
        </div>
      </div>
    );
  }
}
export default SocialLogin;