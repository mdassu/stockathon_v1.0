import Head from 'next/head'
import React, { Component } from 'react';
import { Col, Container, Form, Row, Button } from 'react-bootstrap';
import Layout from "components/Layout/Layout";
import { StockService } from '_services/StockService';
import { ErrorMessages, isNameValid, isMobileNumberValid, isEmailValid } from 'components/Common/Validators';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

class NoStockProfile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      formData: {
        firstName: '',
        lastName: '',
        email: '',
        message: '',
      },
      message: '',
      open: false,
      error: false,
      errorMessage: {
        firstName: '',
        lastName: '',
        email: '',
        message: '',
      },
      isValid: false,
      submitted: false
    }
  }

  componentDidMount() {

  }

  handleFormData = name => ({ target: { value } }) => {
    this.setState({
      formData: {
        ...this.state.formData,
        [name]: value
      }
    }, () => {
      this.checkValidation();
    });

  }

  checkValidation = () => {
    var formData = this.state.formData;

    var msgFirstName = '';
    var msgLastName = '';
    var msgEmailId = '';
    var msgMessage = '';

    var isValid = false;

    // first name validation
    if (formData.firstName) {
      if (!isNameValid(formData.firstName)) {
        msgFirstName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'first');
      }
    } else {
      msgFirstName = 'First name is required';
    }

    // last name validation
    if (formData.lastName) {
      if (!isNameValid(formData.lastName)) {
        msgLastName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'last');
      }
    } else {
      msgLastName = 'Last name is required';
    }

    // email id validation
    if (formData.email) {
      if (!isEmailValid(formData.email)) {
        msgEmailId = (ErrorMessages.VALIDATION_ERROR_EMAIL);
      }
    } else {
      msgEmailId = 'Email id is required';
    }

    if (!formData.message) {
      msgMessage = 'Message is required';
    }

    if (this.state.submitted) {
      this.setState({
        error: true,
        errorMessage: {
          firstName: msgFirstName,
          lastName: msgLastName,
          email: msgEmailId,
          message: msgMessage,
        },
        isValid: false
      });
      if (!msgFirstName && !msgLastName && !msgEmailId && !msgMessage) {
        this.setState({
          isValid: true
        });
      }
    } else {
      if (!msgFirstName && !msgLastName && !msgEmailId && !msgMessage) {
        this.setState({
          isValid: true,
          submitted: true
        });
      }
    }
  }

  submitMessage = (e) => {
    e.preventDefault();
    var formData = this.state.formData;
    var msgFirstName = '';
    var msgLastName = '';
    var msgEmailId = '';
    var msgMessage = '';

    var isValid = false;

    // first name validation
    if (formData.firstName) {
      if (!isNameValid(formData.firstName)) {
        msgFirstName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'first');
      }
    } else {
      msgFirstName = 'First name is required';
    }

    // last name validation
    if (formData.lastName) {
      if (!isNameValid(formData.lastName)) {
        msgLastName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'last');
      }
    } else {
      msgLastName = 'Last name is required';
    }

    // email id validation
    if (formData.email) {
      if (!isEmailValid(formData.email)) {
        msgEmailId = (ErrorMessages.VALIDATION_ERROR_EMAIL);
      }
    } else {
      msgEmailId = 'Email id is required';
    }

    if (!formData.message) {
      msgMessage = 'Message is required';
    }

    if (!msgFirstName && !msgLastName && !msgEmailId && !msgMessage) {
      isValid = true;
    }

    if (isValid) {
      var params = {
        first_name: formData['firstName'],
        last_name: formData['lastName'],
        email: formData['email'],
        message: formData['message']
      }
      StockService.submitRequest(params, res => {
        if (res.status) {
          this.setState({
            formData: {
              firstName: '',
              lastName: '',
              email: '',
              message: '',
            },
            open: true,
            message: 'Your request has been submitted.'
          });
        } else {
          this.setState({
            open: true,
            message: 'Your request has not been submitted.'
          });
        }
      })
    } else {
      this.setState({
        error: true,
        errorMessage: {
          firstName: msgFirstName,
          lastName: msgLastName,
          email: msgEmailId,
          message: msgMessage,
        },
        isValid: true,
      });
    }
  }
  handleClose = () => {
    this.setState({
      open: false
    });
  };

  render() {
    const { formData, errorMessage, message, open, isValid } = this.state;
    const { stockData } = this.props;
    return (
      <React.Fragment>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          open={open}
          autoHideDuration={6000}
          onClose={this.handleClose}
          message={message}
          action={
            <React.Fragment>
              <IconButton size="small" aria-label="close" color="inherit" onClick={this.handleClose}>
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
        <section className="profile-banner-section no-analysis-section">
          <Container>
            {/* Row Start */}
            <Row>
              <Col lg="12" className="py-4">
                <Row>
                  <Col lg="12">
                    <div className="profile-name">
                      {
                        stockData.logo != '' ?
                          (
                            <div className="stock-image">
                              <img src={stockData.logo} alt='Stock-img' title="" />
                            </div>
                          ) : ''
                      }
                      <div className="banner-heading">
                        <h1>{stockData.stock_name} <span>{stockData.symbol}</span> <a href={stockData.weburl} title="Web URL" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-box-arrow-up-right" viewBox="0 0 16 16">
                          <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z" />
                          <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z" />
                        </svg></a></h1>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Container>
        </section>
        <section>
          <Container>
            <Row>
              <Col md="12">
                <div className="no-analysis">
                  <h2>No anlysis available !</h2>
                  <p>There is no analysis available for this stock. Please contact the administrator for more details.</p>
                  <Form onSubmit={this.submitMessage}>
                    <Form.Row>
                      <Col md="4">
                        <Form.Group>
                          <Form.Control placeholder="First name" name="firstName" value={formData.firstName} onChange={this.handleFormData('firstName')} isInvalid={!!errorMessage.firstName} />
                          <Form.Control.Feedback type="invalid">
                            {errorMessage.firstName}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Col>
                      <Col md="4">
                        <Form.Group>
                          <Form.Control placeholder="Last name" name="lastName" value={formData.lastName} onChange={this.handleFormData('lastName')} isInvalid={!!errorMessage.lastName} />
                          <Form.Control.Feedback type="invalid">
                            {errorMessage.lastName}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Col>
                      <Col md="4">
                        <Form.Group>
                          <Form.Control type="email" placeholder="Email" name="email" value={formData.email} onChange={this.handleFormData('email')} isInvalid={!!errorMessage.email} />
                          <Form.Control.Feedback type="invalid">
                            {errorMessage.email}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Col>
                      <Col md="12">
                        <Form.Group>
                          <Form.Control as="textarea" rows={5} placeholder="Message" name="message" value={formData.message} onChange={this.handleFormData('message')} isInvalid={!!errorMessage.message} />
                          <Form.Control.Feedback type="invalid">
                            {errorMessage.message}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Col>
                      <Col md="12">
                        <Button type="submit" disabled={!isValid}>Submit</Button>
                      </Col>
                    </Form.Row>
                  </Form>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </React.Fragment>
    );
  }
}
export default NoStockProfile;