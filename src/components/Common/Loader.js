import { Container, Row } from 'react-bootstrap';

export default function Loader() {
  return (
    <div className="h-100 fullloader">
      <img className="loader-img" src="/images/loader.gif" alt='loader' />
    </div>
  )
}