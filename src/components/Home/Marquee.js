import React, { useState } from 'react';
import Link from 'next/link'
import Ticker, { FinancialTicker, NewsTicker } from 'nice-react-ticker';
const NewMarquee = (props) => {

  const [isActive, setIsActive] = useState(true);
  const [news, setNews] = useState(props.newsList);

  function rand(min, max) {
    var offset = min
    var range = (max - min) + 1
    var randomNumber = Math.floor(Math.random() * range) + offset
    return randomNumber
  }

  function handleMouseEnter(e) {

    setIsActive(false);
  }

  function handleMouseLeave(e) {
    setIsActive(true);

  }

  return (
    <div className="marquee-container" onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
      <div className="marquee">
        <div>
          <Ticker isNewsTicker={true}>
            {
              props.newsList.map((news, key) => {
                return (
                  <NewsTicker id={key} title={news['headline']} url={news['url']} />
                );
              })
            }

            {/* <NewsTicker id="2" title="Blue passports to be issued to Brits for the first time in decades next month" url=" https://metro.co.uk/2020/02/22/blue-passports-issued-brits-first-time-decades-next-months-12281012/?ito=newsnow-feed" meta="11:10:20" />
            <NewsTicker id="3" title="Blue passports to be issued to Brits for the first time in decades next month" url=" https://metro.co.uk/2020/02/22/blue-passports-issued-brits-first-time-decades-next-months-12281012/?ito=newsnow-feed" meta="11:10:20" />
            <NewsTicker id="4" title="Blue passports to be issued to Brits for the first time in decades next month" url=" https://metro.co.uk/2020/02/22/blue-passports-issued-brits-first-time-decades-next-months-12281012/?ito=newsnow-feed" meta="11:10:20" />
            <NewsTicker id="5" title="Blue passports to be issued to Brits for the first time in decades next month" url=" https://metro.co.uk/2020/02/22/blue-passports-issued-brits-first-time-decades-next-months-12281012/?ito=newsnow-feed" meta="11:10:20" />
            <NewsTicker id="6" title="Blue passports to be issued to Brits for the first time in decades next month" url=" https://metro.co.uk/2020/02/22/blue-passports-issued-brits-first-time-decades-next-months-12281012/?ito=newsnow-feed" meta="11:10:20" /> */}
          </Ticker>
        </div>
      </div>
    </div>
  )
}

export default NewMarquee;