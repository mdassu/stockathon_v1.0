import React from 'react'
import { Form, FormControl, Button } from 'react-bootstrap'
import SearchIcon from '@material-ui/icons/Search';
import Link from 'next/link';
import Autocomplete from '../Autocomplete/Autocomplete';
import Marquee from '../Home/Marquee'
import { StockService } from '_services/StockService';
import { CommonService } from '_services/CommonService';


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stockList: [],
      newsList: []
    }
  }

  componentDidMount() {
    this.getPopularList();
    this.getHomeNews();
  }

  getPopularList() {
    StockService.getPopularStock({ type: 'popular' }, res => {
      if (res.status) {
        this.setState({
          stockList: res.data
        });
      } else {
        this.setState({
          stockList: res.data
        });
      }
    });
  }

  getHomeNews = () => {
    CommonService.getHomeNews(res => {
      if (res.status) {
        this.setState({
          newsList: res.data
        })
      } else {
        this.setState({
          newsList: []
        })
      }
    });
  }

  render() {
    const { stockList, newsList } = this.state;
    return <React.Fragment>
      {/* Hompage Main Section */}
      <section className="main-section">
        <div className="container">
          {/* Start Row */}
          <div className="row">
            <div className="col-12">
              <div className="homepage-logo text-center">
                <img src="/images/logo-white.svg" className="desktop-logo" title="Stockathon" alt="stockathon-image" />
                <img src="/images/logo-icon.svg" className="mobile-logo" title="Stockathon" alt="stockathon-image" />
                <p>The best research tool for equity investors</p>
              </div>
            </div>
            <div className="col-12">
              <div className="homepage-search">
                <h1>Search a company</h1>
                <Form className="homepage-form">
                  {/* <FormControl type="text" placeholder="Search" /> */}
                  <Autocomplete />
                  <Button ><SearchIcon /></Button>
                </Form>
              </div>
              <div className="homepage-links" >
                {
                  stockList.map((stock, key) => {
                    return (
                      <Link href={`/company/${stock['symbol']}-stock-price/${stock['label'].replace(/ /g, '-')}`} key={key}><a key={key} title={stock['label']}>{stock['label']}</a></Link>
                    )
                  })
                }
                {/* <Link href="/profile-detail"><a className="view-all-link" title="View All">View All</a></Link> */}
              </div>
              <div className="browse-companies">
                <p>Browse companies by themes and sectors</p>
              </div>
            </div>
          </div>


          {/* End Row */}
        </div>
        {
          newsList && newsList.length > 0 ? (
            <div className="container-fluid">
              <div className="row">
                <div className="col-12">
                  <div className="homepage-news">
                    <Link href="#"><a className="news-heading">News</a></Link>
                    <div className="news-slider">
                      <Marquee newsList={newsList} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : ''
        }
      </section>

    </React.Fragment>
  }
}

export default Home