import React, { Component } from 'react';
import Head from 'next/head';
import { Form, FormControl, Nav, Navbar, NavDropdown, Button, Modal } from 'react-bootstrap';
import SearchIcon from '@material-ui/icons/Search';
import Link from 'next/link'
import Autocomplete from '../Autocomplete/Autocomplete';
import { AuthenticationService } from '_services/AuthenticationService';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Router from 'next/router';
import { CommonService } from '_services/CommonService';
import Cookies from 'js-cookie';
import { encrypt } from '_helper/EncrDecrypt';
import SocialLogin from 'components/Common/SocialLogin';


class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPathname: '',
      show: false
    };
  }

  componentDidMount() {
    this.setState({
      currentPathname: Router.pathname
    })
    this.getMetaData();
  }

  getMetaData() {
    if (!Cookies.get('_metaData')) {
      CommonService.getMetaData(res => {
        if (res.status) {
          Cookies.set('_metaData', encrypt(res.data));
        }
      });
    }
  }

  handleClick = e => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };
  logout = () => {
    AuthenticationService.logout();
  }

  handleClose = () => {
    this.setState({
      show: false
    });
  }

  render() {
    const { open, currentPathname, show } = this.state;
    return (
      <React.Fragment>
        <Head>
          <meta charSet="utf-8" />
          <title>{this.props.title ? this.props.title : 'Best financial tools in the market to analyze and evaluate US stocks. | Stockathon'}</title>
          <link
            rel="icon"
            href="favicon.ico"
            type="image/ico"
            sizes="16x16" />
          <meta
            name="viewport"
            content="width=device-width,  initial-scale=1 maximum-scale=1.0, user-scalable=no" />
          {this.props.metaKeywords ? <meta name="keywords" content={this.props.metaKeywords} /> : ''}
          {this.props.metaDesc ? <meta name="description" content={this.props.metaDesc} /> : ''}
          <link href="https://cdn.syncfusion.com/ej2/material.css" rel="stylesheet"></link>

        </Head>

        {/* Header */}
        <Navbar expand="lg" className={`stockathon-nav ${(currentPathname).search('/company') == -1 ? 'sticky-nav' : ''}`} >
          <Link href="/">
            <a className="navbar-brand" title="Stockathon">
              <img src="/images/logo.svg" width="200" alt="Logo" />
            </a>
          </Link>
          {
            !AuthenticationService.isLogged ? (
              // <Link href="/login">
              <Button className="register-resp" title="Login / Register" onClick={() => {
                this.setState({
                  show: true
                })
              }}>Login / Register</Button>
              // </Link>
            ) : ''
          }
          <Navbar.Toggle aria-controls="basic-navbar-nav" className="x" >
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </Navbar.Toggle>
          <Navbar.Collapse id="basic-navbar-nav">
            <Form inline className="mr-auto header-form">
              {/* <FormControl type="text" placeholder="Search" className="header-search" /> */}
              <div className="header-search">
                <Autocomplete />
              </div>
              <Button className="header-search-btn"><SearchIcon /></Button>
            </Form>
            <Nav>
              <Link href="">
                <a href="/blog" className="nav-link" title="Blog" target="_blank">Blog</a>
              </Link>
              <Link href="">
                <a className="nav-link" title="Screens" onClick={this.handleClick}>Screens</a>
              </Link>

              <Link href="/premium">
                <a className="nav-link" title="Premium">Premium</a>
              </Link>

              <Link href="">
                <a className="nav-link logout" title="Logout" onClick={this.logout}>Logout</a>
              </Link>

              {
                AuthenticationService.isLogged ? (
                  <div className="header-btns">
                    <Button className="header-login-btn" title="Logout" onClick={this.logout} >Logout</Button>
                  </div>
                ) : (
                    <div className="header-btns">
                      {/* <Link href="/login">
                        <Button className="header-login-btn" title="Log In" >Log In</Button>
                      </Link> */}
                      {/* <Link href="/login"> */}
                      <Button className="header-register-btn" title="Login / Register" onClick={() => {
                        this.setState({
                          show: true
                        })
                      }}>Login / Register</Button>
                      {/* </Link> */}
                    </div>
                  )
              }
            </Nav>
          </Navbar.Collapse >
        </Navbar >
        {this.props.children}

        < footer >
          <section className="footer-section">
            <div className="container">
              {/* Start Row */}
              <div className="row">
                <div className="col-12">
                  <div className="footer-links">
                    <ul>
                      <li>
                        <Link href="/"><a title="Home">Home</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="About Us" onClick={this.handleClick}>About Us</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="Support" onClick={this.handleClick}>Support</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="Premium" onClick={this.handleClick}>Premium</a></Link>
                      </li>
                      <li>
                        <Link href="#"><a title="What's New" onClick={this.handleClick}>What's New</a></Link>
                      </li>
                      <li>
                        <Link href="/privacy"><a title="Privacy">Privacy</a></Link>
                      </li>
                      <li>
                        <Link href="/terms"><a title="Terms">Terms</a></Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="footer-copyright">
                  <p>Copyright &copy;2020 <span>Stockathon.</span> All Rights Reserved</p>
                </div>
              </div>
              {/* End Row */}
            </div>
          </section>
        </footer >
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          open={open}
          autoHideDuration={6000}
          onClose={this.handleClose}
          message="Feature coming soon. Stay connected"
          action={
            <React.Fragment>
              <IconButton size="small" aria-label="close" color="inherit" onClick={this.handleClose}>
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
        <Modal show={show} onHide={this.handleClose} className="popup-login" >
          <Modal.Header closeButton className="border-0 p-0">
          </Modal.Header>
          <Modal.Body>
            <SocialLogin hidePopup={() => this.setState({ show: false })} />
          </Modal.Body>
        </Modal>
      </React.Fragment >
    );
  }
}

export default Layout;



