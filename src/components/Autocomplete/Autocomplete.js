import React, { Component, Fragment, useRef } from 'react';
import Router, { withRouter } from 'next/router';


import Form from 'react-bootstrap/Form';
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';


//service import
import { StockService } from '_services/StockService';

class Autocomplete extends Component {

  constructor(props) {
    super(props);
    this.state = {
      stockList: [],
      selected: ''
    };
    this.ref = React.createRef();
  }

  componentDidMount() {
    // this.getStockData();
  }

  getStockData(symbol = '') {
    var params = {
      symbol: symbol
    }
    StockService.getStockList(params, res => {
      if (res.status) {
        this.setState({
          stockList: res.data
        });
      } else {
        this.setState({
          stockList: []
        });
      }
    });
  }

  handleSearchInput = (input, e) => {
    this.getStockData(input);
  }

  redirectToStock = (selectedOption) => {
    var symbol = selectedOption[0]['symbol'];
    var label = selectedOption[0]['label'].replace(/ /g, '-');
    this.ref.current.clear();
    this.ref.current.focus();
    Router.push('/company/' + symbol + '-stock-price/' + label);
  }

  render() {
    return (
      <Fragment>
        <Form.Group>
          <Typeahead
            clearButton
            id="autocomplete"
            minLength='1'
            filterBy={['label', 'symbol']}
            onInputChange={this.handleSearchInput}
            onChange={this.redirectToStock}
            options={this.state.stockList}
            placeholder="Search"
            ref={this.ref}
          />
        </Form.Group>
      </Fragment>
    );
  }
}

export default withRouter(Autocomplete);