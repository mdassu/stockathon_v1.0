import Head from 'next/head'
import React, { Component } from 'react';
import { Button, Table, Container, Row, Col, Modal } from 'react-bootstrap';
import Link from 'next/link'
import { AuthenticationService } from '_services/AuthenticationService';
import SocialLogin from 'components/Common/SocialLogin';


class ProfitAndLoss extends React.Component {
  constructor() {
    super();

    this.state = {
      tableHeader: [],
      addClass: false,
      expandedRows: [],
      colepseIds: [],
      show: false
    };
  }

  componentDidMount() {
    var head = Object.keys(this.props.data[0]);
    if (head.indexOf('innerData') != -1) {
      head.splice(head.indexOf('innerData'), 1);
    }
    if (head.indexOf('type') != -1) {
      head.splice(head.indexOf('type'), 1);
    }
    if (head.indexOf('id') != -1) {
      head.splice(head.indexOf('id'), 1);
    }
    if (head.indexOf('isTotal') != -1) {
      head.splice(head.indexOf('isTotal'), 1);
    }
    this.setState({
      tableHeader: head
    });
  }

  // componentDidUpdate(){
  //   var head = Object.keys(this.props.data[0]);
  //   if (head.indexOf('innerData') != -1) {
  //     head.splice(head.indexOf('innerData'), 1);
  //   }
  //   if (head.indexOf('type') != -1) {
  //     head.splice(head.indexOf('type'), 1);
  //   }
  //   if (head.indexOf('id') != -1) {
  //     head.splice(head.indexOf('id'), 1);
  //   }
  //   if (head.indexOf('isTotal') != -1) {
  //     head.splice(head.indexOf('isTotal'), 1);
  //   }
  //   this.setState({
  //     tableHeader: head
  //   });
  // }

  toggle = (id) => {
    var colIndex = this.state.colepseIds.indexOf(id);
    var colIds = this.state.colepseIds;
    if (this.state.colepseIds.indexOf(id) != -1) {
      colIds.splice(colIndex, 1);
    } else {
      colIds.push(id);
    }
    this.setState({ colepseIds: colIds });

  }

  handleRowClick(rowId) {
    const currentExpandedRows = this.state.expandedRows;
    const isRowCurrentlyExpanded = currentExpandedRows.includes(rowId);

    const newExpandedRows = isRowCurrentlyExpanded ?
      currentExpandedRows.filter(id => id !== rowId) :
      currentExpandedRows.concat(rowId);

    this.setState({ expandedRows: newExpandedRows });
  }

  renderItem(item) {
    let boxClass = [""];
    if (this.state.addClass) {
      boxClass.push('active-tr');
    }
    const clickCallback = () => {
      if (AuthenticationService.isLogged) {
        var user = AuthenticationService.currentUserValue;
        if (user['membership_flag'] == 1) {
          this.handleRowClick(item.id);
          this.toggle(item.id);
        } else {
          Router.push('/premuim');
        }
      } else {
        this.setState({
          show: true
        });
      }
    };
    // var head = this.state.tableHeader;
    // if (head.indexOf('id') != -1) {
    //   head.splice(head.indexOf('id'), 1);
    // }
    // this.setState({
    //   tableHeader: head
    // });
    const itemRows = [
      // <tr className={this.state.colepseIds.indexOf(item.id) != -1 ? 'active-tr' : item.innerData.length > 0 ? 'expend-active' : ''} >
      <tr className={`${this.state.colepseIds.indexOf(item.id) != -1 ? 'active-tr' : ''} ${item.innerData.length > 0 ? 'expend-active' : ''} ${item.isTotal == 1 ? 'sum-tr' : ''}`} >
        <td onClick={item.innerData.length > 0 ? clickCallback : ''} key={"row-data-" + item.id} className="cursor">{item.type} {item.innerData.length > 0 ? (<Button></Button>) : ''}</td>
        {
          this.state.tableHeader.map((head, key) => {
            return (<td key={key}>{item[head]}</td>);
          })
        }
      </tr >,
    ];
    if (this.state.expandedRows.includes(item.id)) {
      itemRows.push(
        <tbody className="inner-tbody">
          {
            item.innerData.map(innerItem => {
              return (<tr key={"row-expanded-" + item.id}>
                <td>{innerItem.type}</td>
                {
                  this.state.tableHeader.map((head, key) => {
                    return (<td key={key}>{innerItem[head]}</td>);
                  })
                }
              </tr>)
            })
          }
        </tbody>
      );
    }

    return itemRows;
  }

  handleClose = () => {
    this.setState({
      show: false
    });
  }


  render() {
    const { show } = this.state;
    let allItemRows = [];
    this.props.data.map((itemData, key) => {
      var item = itemData;
      item['id'] = key;
      const perItemRows = this.renderItem(item);
      allItemRows = allItemRows.concat(perItemRows);
    });

    return (
      <div className="table-responsive">
        <Table>
          <thead>
            <tr>
              <th></th>
              {
                this.state.tableHeader.map((head, key) => {
                  return (<th key={key}>{head}</th>);
                })
              }
            </tr>
          </thead>
          {allItemRows}
        </Table>
        <Modal show={show} onHide={this.handleClose} className="popup-login" >
          <Modal.Header closeButton className="border-0 p-0">
          </Modal.Header>
          <Modal.Body>
            <SocialLogin hidePopup={() => this.setState({ show: false })} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
export default ProfitAndLoss;